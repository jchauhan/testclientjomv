/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import java.util.Observable;

import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.om.TerseObjectUpdateEventArgs;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector2;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class FlyToCommand extends Command
    {

        Vector3 myPos = new Vector3();
        Vector2 myPos0 = new Vector2();
        Vector3 target = new Vector3();
        Vector2 target0 = new Vector2();
        float diff, olddiff, saveolddiff;
        long startTime = 0;
        int duration = 10000;
        boolean running = false;
        EventObserver<TerseObjectUpdateEventArgs> TerseObjectUpdateEO; 
        
        public FlyToCommand(TestClient Client)
        {
        	super(Client);
            Name = "FlyTo";
            Description = "Fly the avatar toward the specified position for a maximum of seconds. Usage: FlyTo x y z [seconds]";
            Category = CommandCategory.Movement;
            TerseObjectUpdateEO = new EventObserver<TerseObjectUpdateEventArgs>()
    		{
				@Override
				public void handleEvent(Observable o, TerseObjectUpdateEventArgs arg) {
					Objects_OnObjectUpdated(o, arg);
				}
				};
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID)
        {
            if (args.length > 4 || args.length < 3)
                return "Usage: FlyTo x y z [seconds]";

            float[] X = new float[1];
            float[] Y = new float[1];
            float[] Z = new float[1];
            
            if (!Utils.tryParseFloat(args[0], X) ||
                !Utils.tryParseFloat(args[1], Y) ||
                !Utils.tryParseFloat(args[2], Z))
            {
                return "Usage: FlyTo x y z [seconds]";
            }

            target.X = X[0];
            target.Y = Y[0];
            target.Z = Z[0];
            
            if (running)
                return "Already in progress, wait for the previous FlyTo to finish";

            running = true;

            // Subscribe to terse update events while this command is running
            Client.objects.registerOnTerseObjectUpdate(TerseObjectUpdateEO);

            target0.X = target.X;
            target0.Y = target.Y;

            int[] da = new int[1]; 
            if (args.length == 4 && Utils.tryParseInt(args[3], da))
            {
            	duration = da[0];
                duration *= 1000;
            }

            startTime = Utils.getUnixTime();
            Client.self.Movement.setFly(true);
            Client.self.Movement.setAtPos(true);
            Client.self.Movement.setAtNeg(false);
            ZMovement();
            Client.self.Movement.TurnToward(target);

            return String.format("flying to %s in %d seconds", 
            		target.toRawString(), duration / 1000);
        }

        private void Objects_OnObjectUpdated(Object sender, TerseObjectUpdateEventArgs e)
        {
            if (startTime == 0) return;
            if (e.getUpdate().LocalID == Client.self.getLocalID())
            {
                XYMovement();
                ZMovement();
                if (Client.self.Movement.getAtPos() || Client.self.Movement.getAtNeg())
                {
                    Client.self.Movement.TurnToward(target);
                    Debug("Flyxy ");
                }
                else if (Client.self.Movement.getUpPos() || Client.self.Movement.getUpNeg())
                {
                    Client.self.Movement.TurnToward(target);
                    //Client.Self.Movement.SendUpdate(false);
                    Debug("Fly z ");
                }
                else if (Vector3.distance(target, Client.self.getSimPosition()) <= 2.0)
                {
                    EndFlyto();
                    Debug("At Target");
                }
            }
            
            if (Utils.getUnixTime() - startTime > duration)
            {
                EndFlyto();
                Debug("End Flyto");
            }
        }

        private boolean XYMovement()
        {
            boolean res = false;

            myPos = Client.self.getSimPosition();
            myPos0.X = myPos.X;
            myPos0.Y = myPos.Y;
            diff = Vector2.distance(target0, myPos0);
            Vector2 vvel = new Vector2(Client.self.getVelocity().X, Client.self.getVelocity().Y);
            float vel = vvel.length();
            if (diff >= 10.0)
            {
                Client.self.Movement.setAtPos(true);
                
                res = true;
            }
            else if (diff >= 2 && vel < 5)
            {
                Client.self.Movement.setAtPos(true);
            }
            else
            {
                Client.self.Movement.setAtPos(false);
                Client.self.Movement.setAtNeg(false);
            }
            saveolddiff = olddiff;
            olddiff = diff;
            return res;
        }

        private void ZMovement()
        {
            Client.self.Movement.setUpPos(false);
            Client.self.Movement.setUpNeg(false);
            float diffz = (target.Z - Client.self.getSimPosition().Z);
            if (diffz >= 20.0)
                Client.self.Movement.setUpPos(true);
            else if (diffz <= -20.0)
                Client.self.Movement.setUpNeg(true);
            else if (diffz >= +5.0 && Client.self.getVelocity().Z < +4.0)
                Client.self.Movement.setUpPos(true);
            else if (diffz <= -5.0 && Client.self.getVelocity().Z > -4.0)
                Client.self.Movement.setUpNeg(true);
            else if (diffz >= +2.0 && Client.self.getVelocity().Z < +1.0)
                Client.self.Movement.setUpPos(true);
            else if (diffz <= -2.0 && Client.self.getVelocity().Z > -1.0)
                Client.self.Movement.setUpNeg(true);
        }

        private void EndFlyto()
        {
            // Unsubscribe from terse update events
        	
            Client.objects.unregisterOnTerseObjectUpdate(TerseObjectUpdateEO);

            startTime = 0;
            Client.self.Movement.setAtPos(false);
            Client.self.Movement.setAtNeg(false);
            Client.self.Movement.setUpPos(false);
            Client.self.Movement.setUpNeg(false);
            Client.self.Movement.SendUpdate(false);

            running = false;
        }

//        [System.Diagnostics.Conditional("DEBUG")]
        private void Debug(String x)
        {
            JLogger.debug(String.format(x + " {0,3:##0} {1,3:##0} {2,3:##0} diff {3,5:##0.0} olddiff {4,5:##0.0}  At:{5,5} {6,5}  Up:{7,5} {8,5}  v: {9} w: {10}",
                myPos.X, myPos.Y, myPos.Z, diff, saveolddiff,
                Client.self.Movement.getAtPos(), Client.self.Movement.getAtNeg(), Client.self.Movement.getUpPos(), Client.self.Movement.getUpNeg(),
                Client.self.getVelocity().toRawString(), Client.self.getAngularVelocity().toString()));
        }
    }