/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.rendering;

import java.util.List;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.rendering.FaceData;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderPrimitive;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureLoadItem;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureManagerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.DetailLevel;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primitive.PrimitiveMesher;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class CopyOfDrawPrimitiveCommand extends Command
{
	TextureManagerR textureManager  = null;
	public CopyOfDrawPrimitiveCommand(TestClient testClient)
	{
		super(testClient);
		Name = "drawprimitive";
		Description = "Primitive Rendering Example";
		Category = CommandCategory.Rendering;    
		textureManager = new TextureManagerR(Client);
	}


	@Override
	public  String Execute(String[] args, UUID fromAgentID) throws Exception
	{
		final StringBuilder result = new StringBuilder();

		textureManager.start();
		
		Simulator sim = Client.network.getCurrentSim();
		PrimitiveMesher primMesher = new PrimitiveMesher(Client); 
		List<Primitive> mainPrims = sim.ObjectsPrimitives.FindAll(new Predicate<Primitive>()
				{
			public boolean match(Primitive p) 
			{
				return p.ParentID == 0;
			}	
				});

		for (Primitive mainPrim : mainPrims)
		{
			//TODO 
			RenderPrimitive rmainprim = UpdatePrimBlocking(primMesher, mainPrim);
			final long rootPrimId = mainPrim.LocalID;
			System.out.println(mainPrim.LocalID + " " + mainPrim.getType() + " " + mainPrim.PrimData.PCode);
			printTextures(rmainprim, mainPrim, "\t");
			List<Primitive> childPrims = sim.ObjectsPrimitives.FindAll(new Predicate<Primitive>()
					{
				public boolean match(Primitive t) {
					return t.ParentID == rootPrimId;
				}
					});

			for(Primitive subPrim: childPrims)
			{   
				System.out.println("\t\t" + subPrim.LocalID + " " + subPrim.getType() + " " + subPrim.PrimData.PCode);
				RenderPrimitive rsubprim = UpdatePrimBlocking(primMesher, subPrim);
				printTextures(rsubprim, subPrim, "\t\t\t");
			}
		}

		return result.toString();
	}

	public RenderPrimitive UpdatePrimBlocking(PrimitiveMesher primMesher, Primitive prim) throws Exception
	{

		RenderPrimitive rPrim = new RenderPrimitive();
		rPrim.setBasePrim(prim);
		rPrim.Meshed = false;
		rPrim.BoundingVolume = new BoundingVolume();
		rPrim.BoundingVolume.FromScale(prim.Scale);         
		primMesher.MeshPrim(rPrim, false, DetailLevel.High, DetailLevel.High, DetailLevel.High);
		return rPrim;
	}

	void printTextures(RenderPrimitive rprim, Primitive prim, String indent) throws Exception
	{
		for(int i =0;  i < rprim.Faces.size(); i ++)
		{
			TextureEntryFace tef = prim.Textures.GetFace(i);
			System.out.println(String.format(indent + "Face Index: %d, Texture ID: %s", i, tef.getTextureID()));
			if(!Client.assets.Cache.hasAsset(tef.getTextureID()))
			{
				System.out.println(indent + "Downloading textures ... ");
				Client.assets.RequestImage(tef.getTextureID(), getTextureDownloadCallback(tef.getTextureID().toString()));
			}
			textureManager.requestDownloadTexture(new TextureLoadItem((FaceData)rprim.Faces.get(i).UserData, 
					prim, prim.Textures.GetFace(i)));
		}
	}
	
	public MethodDelegate<Void, TextureDownloadCallbackArgs>  getTextureDownloadCallback(final String filename)
	{
		return new MethodDelegate<Void, TextureDownloadCallbackArgs>()
		{
			public Void execute(TextureDownloadCallbackArgs e) 
			{
				switch (e.getState())
				{
				case Finished:
					Client.assets.Cache.saveAssetToCache(filename, e.getAssetTexture().AssetData);
					break;

				case Aborted:
				case NotFound:
				case Timeout:
					break;
				}
				return null;
			}

		};
	}

}
