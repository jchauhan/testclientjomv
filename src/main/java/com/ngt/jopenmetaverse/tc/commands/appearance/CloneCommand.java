/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.appearance;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import com.ngt.jopenmetaverse.shared.protocol.AgentSetAppearancePacket;
import com.ngt.jopenmetaverse.shared.protocol.AvatarAppearancePacket;
import com.ngt.jopenmetaverse.shared.sim.DirectoryManager;
import com.ngt.jopenmetaverse.shared.sim.DirectoryManager.AgentSearchData;
import com.ngt.jopenmetaverse.shared.sim.DirectoryManager.DirFindFlags;
import com.ngt.jopenmetaverse.shared.sim.events.AutoResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.dm.DirPeopleReplyEventArgs;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryItem;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


public class CloneCommand extends Command
    {
		//uint
        long SerialNum = 2;

        public CloneCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "clone";
            Description = "Clone the appearance of a nearby avatar. Usage: clone [name]";
            Category = CommandCategory.Appearance;
        }

    	    private List<AgentSearchData> PeopleSearch(DirFindFlags findFlags, String searchText, 
    	    		int queryStart, int timeoutMS) throws InterruptedException
    	    {
    	        final AutoResetEvent searchEvent = new AutoResetEvent(false);
    	        final UUID[] id = new UUID[] {UUID.Zero};
    	        List<AgentSearchData> people = null;
    	
    	        final Object[] returnData = new Object[1];
    	        EventObserver<DirPeopleReplyEventArgs> callback 
    	        	= new EventObserver<DirPeopleReplyEventArgs>()
    	        	{
						@Override
						public void handleEvent(Observable sender,
								DirPeopleReplyEventArgs e) {
	    	                if (id[0].equals(e.getQueryID()))
	    	                {
	    	                    returnData[0] = e.getMatchedPeople();
	    	                    searchEvent.set();
	    	                }
	    	                System.out.println("Got People Search Reply: " + e.getMatchedPeople().get(0).FirstName);
						}
    	        	};
    	      
    	        Client.directory.registerOnDirPeopleReply(callback);
//    	        DirPeopleReply += callback;
    	        
    	        id[0] = Client.directory.StartPeopleSearch(searchText, queryStart);
    	        if(!searchEvent.waitOne(timeoutMS))
    	        {
    	        	JLogger.debug("People Search Timeout Hanppened");
    	        	System.out.println("People Search Timeout Hanppened");
    	        }
    	        	
    	        Client.directory.unregisterOnDirPeopleReply(callback);
    	
    	        return people;
    	    }
        
        @Override
        public String Execute(String[] args, UUID fromAgentID) throws Exception
        {
            String targetName = "";
            List<DirectoryManager.AgentSearchData> matches;

            for (int ct = 0; ct < args.length; ct++)
                targetName = targetName + args[ct] + " ";
            targetName = targetName.trim();

            if (targetName.length() == 0)
                return "Usage: clone [name]";
            
            
            if ((matches = PeopleSearch(DirectoryManager.DirFindFlags.People, targetName, 0, 1000 * 10))!=null
            		&& matches.size() > 0)
            {
                UUID target = matches.get(0).AgentID;
                targetName += String.format(" (%s)", target);

                if (Client.Appearances.containsKey(target))
                {
                    //region AvatarAppearance to AgentSetAppearance

                    AvatarAppearancePacket appearance = Client.Appearances.get(target);

                    AgentSetAppearancePacket set = new AgentSetAppearancePacket();
                    set.AgentData.AgentID = Client.self.getAgentID();
                    set.AgentData.SessionID = Client.self.getSessionID();
                    set.AgentData.SerialNum = SerialNum++;
                    set.AgentData.Size = new Vector3(2f, 2f, 2f); // HACK

                    set.WearableData = new AgentSetAppearancePacket.WearableDataBlock[0];
                    set.VisualParam = new AgentSetAppearancePacket.VisualParamBlock[appearance.VisualParam.length];

                    for (int i = 0; i < appearance.VisualParam.length; i++)
                    {
                        set.VisualParam[i] = new AgentSetAppearancePacket.VisualParamBlock();
                        set.VisualParam[i].ParamValue = appearance.VisualParam[i].ParamValue;
                    }

                    set.ObjectData.TextureEntry = appearance.ObjectData.TextureEntry;

                    //endregion AvatarAppearance to AgentSetAppearance

                    // Detach everything we are currently wearing
                    Client.appearance.AddAttachments(new ArrayList<InventoryItem>(), true);

                    // Send the new appearance packet
                    Client.network.SendPacket(set);

                    return "Cloned " + targetName;
                }
                else
                {
                    return "Don't know the appearance of avatar " + targetName;
                }
            }
            else
            {
                return "Couldn't find avatar " + targetName;
            }
        }
    }