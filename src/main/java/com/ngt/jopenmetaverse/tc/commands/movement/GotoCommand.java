/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;
import com.ngt.jopenmetaverse.tc.TestClient;

 public class GotoCommand extends Command
    {
        public GotoCommand(TestClient testClient)
		{
        	super(testClient);
			Name = "goto";
			Description = "Teleport to a location (e.g. \"goto Hooper/100/100/30\")";
            Category = CommandCategory.Movement;
		}

        @Override
        public String Execute(String[] args, UUID fromAgentID) throws InterruptedException
		{
			if (args.length < 1)
                return "Usage: goto sim/x/y/z";

            String destination = "";

            // Handle multi-word sim names by combining the arguments
            for (String arg : args)
            {
                destination += arg + " ";
            }
            destination = destination.trim();

            String[] tokens = destination.split("/");
            if (tokens.length != 4)
                return "Usage: goto sim/x/y/z";

            String sim = tokens[0];
            float[] x= new float[1];
            float[] y = new float[1];
            float[] z = new float[1];
            if (!Utils.tryParseFloat(tokens[1], x) ||
                !Utils.tryParseFloat(tokens[2], y) ||
                !Utils.tryParseFloat(tokens[3], z))
            {
                return "Usage: goto sim/x/y/z";
            }

            if (Client.self.Teleport(sim, new Vector3(x[0], y[0], z[0])))
                return "Teleported to " + Client.network.getCurrentSim();
            else
                return "Teleport failed: " + Client.self.getTeleportMessage();
		}
    }