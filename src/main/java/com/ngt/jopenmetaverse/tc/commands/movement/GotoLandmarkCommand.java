/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;
import com.ngt.jopenmetaverse.tc.TestClient;

 public class GotoLandmarkCommand extends Command
    {
        public GotoLandmarkCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "goto_landmark";
            Description = "Teleports to a Landmark. Usage: goto_landmark [UUID]";
            Category = CommandCategory.Movement;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID) throws InterruptedException
        {
            if (args.length < 1)
            {
                return "Usage: goto_landmark [UUID]";
            }

            UUID[] landmark = new UUID[]{new UUID()};
            if (!Utils.tryParseUUID(args[0],  landmark))
            {
                return "Invalid LLUID";
            }
            else
            {
                System.out.println("Teleporting to " + landmark[0].toString());
            }
            if (Client.self.Teleport(landmark[0]))
            {
                return "Teleport Succesful";
            }
            else
            {
                return "Teleport Failed";
            }
        }
    }