/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.system;

import java.util.List;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class HelpCommand extends Command
{
    public HelpCommand(TestClient testClient)
	{
    	super(testClient);
		Name = "help";
		Description = "Lists available commands. usage: help [command] to display information on commands";
        Category = CommandCategory.TestClient;
	}

    @Override
    public String Execute(String[] args, UUID fromAgentID)
	{
        if (args.length > 0)
        {
            if (Client.commands.contains(args[0]))
                return Client.commands.get(args[0]).Description;
            else
                return "Command " + args[0] + " Does not exist. \"help\" to display all available commands.";
        }
		StringBuilder result = new StringBuilder();
		        
        for(CommandCategory cat: Client.commands.getCommandCategoryKeys())
        {
            result.append(String.format("\n* %s Related Commands:\n", cat.toString()));
        	List<Command> cmdList = Client.commands.get(cat);
            int colMax = 0;
        	for(Command cmd: cmdList)
        	{
                  if (colMax >= 120)
                  {
                      result.append('\n');
                      colMax = 0;
                  }
  
                  result.append(String.format(" %s ", cmd.Name));
                  colMax += 15;
        	}
        }
        
//
//        CommandCategory cc;
//		foreach (Command c in Client.Commands.Values)
//		{
//            if (c.Category.Equals(null))
//                cc = CommandCategory.Unknown;
//            else
//                cc = c.Category;
//
//            if (CommandTree.ContainsKey(cc))
//                CommandTree[cc].Add(c);
//            else
//            {
//                List<Command> l = new List<Command>();
//                l.Add(c);
//                CommandTrepublic class HelpCommand {
//
//}e.Add(cc, l);
//            }
//		}
//
//        foreach (KeyValuePair<CommandCategory, List<Command>> kvp in CommandTree)
//        {
//            result.AppendFormat(System.Environment.NewLine + "* {0} Related Commands:" + System.Environment.NewLine, kvp.Key.ToString());
//            int colMax = 0;
//            for (int i = 0; i < kvp.Value.Count; i++)
//            {
//                if (colMax >= 120)
//                {
//                    result.AppendLine();
//                    colMax = 0;
//                }
//
//                result.AppendFormat(" {0,-15}", kvp.Value[i].Name);
//                colMax += 15;
//            }
//            result.AppendLine();
//        }
//        result.AppendLine(System.Environment.NewLine + "Help [command] for usage/information");
        
        return result.toString();
	}
}
