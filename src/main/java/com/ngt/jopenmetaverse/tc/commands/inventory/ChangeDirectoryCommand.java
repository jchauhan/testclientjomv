/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.inventory;

import java.util.List;

import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;
import com.ngt.jopenmetaverse.shared.exception.nm.InventoryException;
import com.ngt.jopenmetaverse.shared.sim.InventoryManager.InventoryFolder;
import com.ngt.jopenmetaverse.shared.sim.inventory.Inventory;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryBase;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.Utils;

 public class ChangeDirectoryCommand extends Command
    {
        private Inventory Inventory;

        public ChangeDirectoryCommand(TestClient client)
        {
        	super(client);
            Name = "cd";
            Description = "Changes the current working inventory folder.";
            Category = CommandCategory.Inventory;
        }
        @Override
        public String Execute(String[] args, UUID fromAgentID) throws InventoryException
        {
            Inventory = Client.inventory.getStore();

            if (args.length > 1)
                return "Usage: cd [path-to-folder]";
            String pathStr = "";
            String[] path = null;
            if (args.length == 0)
            {
                path = new String[] { "" };
                // cd without any arguments doesn't do anything.
            }
            else if (args.length == 1)
            {
                pathStr = args[0];
                path = pathStr.split("/");
                // Use '/' as a path seperator.
            }
            InventoryFolder currentFolder = Client.CurrentDirectory;
            if (pathStr.startsWith("/"))
                currentFolder = Inventory.getRootFolder();

            if (currentFolder == null) // We need this to be set to something. 
                return "Error: Client not logged in.";

            // Traverse the path, looking for the 
            for (int i = 0; i < path.length; ++i)
            {
                String nextName = path[i];
                if (Utils.isNullOrEmpty(nextName) || nextName.equals("."))
                    continue; // Ignore '.' and blanks, stay in the current directory.
                if (nextName.equals("..") && !currentFolder.equals(Inventory.getRootFolder()))
                {
                    // If we encounter .., move to the parent folder.
                    currentFolder = (InventoryFolder)Inventory.get(currentFolder.ParentUUID);
                }
                else
                {
                    List<InventoryBase> currentContents = Inventory.GetContents(currentFolder);
                    // Try and find an InventoryBase with the corresponding name.
                    boolean found = false;
                    for (InventoryBase item : currentContents)
                    {
                        // Allow lookup by UUID as well as name:
                        if (item.Name.equals(nextName) || item.UUID.toString().equals(nextName))
                        {
                            found = true;
                            if (item instanceof InventoryFolder)
                            {
                                currentFolder = (InventoryFolder)item;
                            }
                            else
                            {
                                return item.Name + " is not a folder.";
                            }
                        }
                    }
                    if (!found)
                        return nextName + " not found in " + currentFolder.Name;
                }
            }
            Client.CurrentDirectory = currentFolder;
            return "Current folder: " + currentFolder.Name;
        }
    }