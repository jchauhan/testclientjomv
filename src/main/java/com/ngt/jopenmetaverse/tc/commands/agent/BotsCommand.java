/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.agent;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;

import com.ngt.jopenmetaverse.shared.sim.Avatar;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.avm.ViewerEffectEventArgs;
import com.ngt.jopenmetaverse.shared.sim.events.avm.ViewerEffectLookAtEventArgs;
import com.ngt.jopenmetaverse.shared.sim.events.avm.ViewerEffectPointAtEventArgs;
import com.ngt.jopenmetaverse.shared.types.Action;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


 public class BotsCommand extends Command
    {
        private Map<UUID, Boolean> m_AgentList = new HashMap<UUID, Boolean>();

        public BotsCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "bots";
            Description = "detects avatars that appear to be bots.";
            Category = CommandCategory.Other;        
//            testClient.avatars.ViewerEffect += new EventHandler<ViewerEffectEventArgs>(Avatars_ViewerEffect);
//            testClient.avatars.ViewerEffectLookAt += new EventHandler<ViewerEffectLookAtEventArgs>(Avatars_ViewerEffectLookAt);
//            testClient.avatars.ViewerEffectPointAt += new EventHandler<ViewerEffectPointAtEventArgs>(Avatars_ViewerEffectPointAt);
            testClient.avatars.registerOnViewerEffect(new EventObserver<ViewerEffectEventArgs>()
            		{
						@Override
						public void handleEvent(Observable o,
								ViewerEffectEventArgs arg) {
							Avatars_ViewerEffect(o, arg);
						}
            		}
            );
            testClient.avatars.registerOnViewerEffectLookAt(new EventObserver<ViewerEffectLookAtEventArgs>()
            		{
						@Override
						public void handleEvent(Observable o,
								ViewerEffectLookAtEventArgs arg) {
							Avatars_ViewerEffectLookAt(o, arg);
						}
            		});
            testClient.avatars.registerOnViewerEffectPointAt(new EventObserver<ViewerEffectPointAtEventArgs>()
            		{
						@Override
						public void handleEvent(Observable o,
								ViewerEffectPointAtEventArgs arg) {
							Avatars_ViewerEffectPointAt(o, arg);
						}
            		});
        }
        
        private void Avatars_ViewerEffectPointAt(Object sender, ViewerEffectPointAtEventArgs e)
        {
            synchronized (m_AgentList)
            {
                if (m_AgentList.containsKey(e.getSourceID()))
                    m_AgentList.put(e.getSourceID(), true);
                else
                    m_AgentList.put(e.getSourceID(), true);
            }
        }

        private void Avatars_ViewerEffectLookAt(Object sender, ViewerEffectLookAtEventArgs e)
        {
            synchronized (m_AgentList)
            {
                if (m_AgentList.containsKey(e.getSourceID()))
                    m_AgentList.put(e.getSourceID(), true);
                else
                    m_AgentList.put(e.getSourceID(), true);
            }
        }

        private void Avatars_ViewerEffect(Object sender, ViewerEffectEventArgs e)
        {
            synchronized (m_AgentList)
            {
                if (m_AgentList.containsKey(e.getSourceID()))
                    m_AgentList.put(e.getSourceID(), true);
                else
                    m_AgentList.put(e.getSourceID(), true);
            }
        }
        
        @Override
        public  String Execute(String[] args, UUID fromAgentID)
        {
            final StringBuilder result = new StringBuilder();

            synchronized (Client.network.Simulators)
            {
                for (int i = 0; i < Client.network.Simulators.size(); i++)
                {
                	Client.network.Simulators.get(i).ObjectsAvatars.foreach(new Action<Entry<Long, Avatar>>(){
    					public void execute(Entry<Long, Avatar> e) {
    						Avatar av = e.getValue();
    						synchronized (m_AgentList)
                            {
                                if (!m_AgentList.containsKey(av.ID))
                                {
                                    result.append(String.format("\n%s %s %s (Group: %s, Location: %s, UUID: %s LocalID: %d) Is Probably a bot",
                                        av.getFirstName(), av.getLastName(), av.getName(), av.getGroupName(), av.Position.toString(), av.ID.toString(), av.LocalID));
                                }
                            }
    					}	
                    }
                    );
                	
//                    Client.network.Simulators.get(i).ObjectsAvatars.ForEach(
//                        delegate(Avatar av)
//                        {
//                            synchronized (m_AgentList)
//                            {
//                                if (!m_AgentList.ContainsKey(av.ID))
//                                {
//                                    result.AppendLine();
//                                    result.AppendFormat("{0} (Group: {1}, Location: {2}, UUID: {3} LocalID: {4}) Is Probably a bot",
//                                        av.Name, av.GroupName, av.Position, av.ID, av.LocalID);
//                                }
//                            }
//                        }
//                    );
                }
            }

            return result.toString();
        }
    }