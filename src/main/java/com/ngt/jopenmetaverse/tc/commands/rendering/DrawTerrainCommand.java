/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.rendering;

import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.ColorVertex;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.InterleavedVBO;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.MeshmerizerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient.DefaultTerrainWindow;
import com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient.DefaultTerrainWindowV2;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainHelper;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainSplat;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class DrawTerrainCommand extends Command
{
    public DrawTerrainCommand(TestClient testClient)
    {
    	super(testClient);
        Name = "drawterrain";
        Description = "Terrain Rendering Example";
        Category = CommandCategory.Rendering;        
    }
    
    
    @Override
    public  String Execute(String[] args, UUID fromAgentID) throws Exception
    {
        final StringBuilder result = new StringBuilder();
        
	       Simulator sim = Client.network.getCurrentSim();
		   
	       float[][] heightMap = TerrainHelper.createHeightTable(sim.Terrain, null);
	       UUID[] textureIds = new UUID[] { sim.TerrainDetail0, sim.TerrainDetail1, sim.TerrainDetail2, sim.TerrainDetail3 };
	       float[] startHeights = new float[] { sim.TerrainStartHeight00, sim.TerrainStartHeight01, sim.TerrainStartHeight10, sim.TerrainStartHeight11 };
	       float[] heightRanges = new float[] { sim.TerrainHeightRange00, sim.TerrainHeightRange01, sim.TerrainHeightRange10, sim.TerrainHeightRange11 };
	       
	       IBitmap bitmap = TerrainSplat.Splat(Client, heightMap, textureIds, startHeights, heightRanges);
        
	       if(bitmap == null)
	    	   return "bitmap is null";
	       
	       System.out.println(String.format("Image splated Width %d, Height %d", bitmap.getWidth(), bitmap.getHeight()));
	       
	       
	       MeshmerizerR renderer = new MeshmerizerR();
           Face terrainFace = renderer.TerrainMesh(heightMap, 0f, 255f, 0f, 255f);
           ColorVertex[] terrainVertices = TerrainHelper.genColorVertices(terrainFace);
           short[] terrainIndices = terrainFace.toIndicesArray();
           byte[] data = ColorVertex.toBytes(terrainVertices);
           InterleavedVBO vertexVBO = new InterleavedVBO(data, ColorVertex.Size, 3, 3, 
				2, 0, 12, 24);
           
//           System.out.println("Indices Array");
//           print(terrainIndices);
//
//           System.out.println("Color Vertex Array");
//           print(terrainVertices);
           
//           DefaultTerrainWindow demo = new DefaultTerrainWindow(bitmap, vertexVBO, terrainIndices); 
           DefaultTerrainWindowV2 demo = new DefaultTerrainWindowV2(Client, bitmap, vertexVBO, terrainIndices); 

           // set title, window size
           demo.window_title = "Current Terrain";
           demo.displayWidth = 640;
           demo.displayHeight = 480;
    	
    	// start running: will call init(), setup(), draw(), mouse functions
    	demo.run();
		return result.toString();
    }
    

	private static void print(ColorVertex[] array)
	{
		int count = 0;
		while(count < array.length)
		{
//			System.out.println("");
			for(int x =0 ; x < 50 && count <  array.length	 ; x++)
			{
				System.out.println(String.format("Index %d Vertex: %s", count, array[count].toString()));
				count ++;
			}
		}
	}
    
    
	private static void print(short[] array)
	{
		int count = 0;
		while(count < array.length)
		{
			System.out.println("");
			for(int x =0 ; x < 50 && count <  array.length	 ; x++)
			{
				System.out.print((int)array[count++] + " ");
			}
		}
	}
    
}