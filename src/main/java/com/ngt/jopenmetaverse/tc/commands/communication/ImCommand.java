/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.communication;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;

import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.ManualResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.avm.AvatarPickerReplyEventArgs;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class ImCommand extends Command
{
    String ToAvatarName = "";
    ManualResetEvent NameSearchEvent = new ManualResetEvent(false);
    Map<String, UUID> Name2Key = new HashMap<String, UUID>();

    public ImCommand(TestClient testClient)
    {
    	super(testClient);
//        testClient.avatars.AvatarPickerReply += Avatars_AvatarPickerReply;
        testClient.avatars.registerOnAvatarPickerReply(new EventObserver<AvatarPickerReplyEventArgs>()
        		{
					@Override
					public void handleEvent(Observable o,
							AvatarPickerReplyEventArgs arg) {
						Avatars_AvatarPickerReply(o, arg);					
						}
        		});

        Name = "im";
        Description = "Instant message someone. Usage: im [firstname] [lastname] [message]";
        Category = CommandCategory.Communication;
    }
    
    @Override
    public String Execute(String[] args, UUID fromAgentID)
    {
        if (args.length < 3)
            return "Usage: im [firstname] [lastname] [message]";

        ToAvatarName = args[0] + " " + args[1];
        
        // Build the message
        String message = "";
        for (int ct = 2; ct < args.length; ct++)
            message += args[ct] + " ";
//        message = message.TrimEnd();
        message = message.trim();
        if (message.length() > 1023) message = message.substring(0, 1023);

        System.out.println("To Avatar: " + ToAvatarName + "\n Meesage: " + message);
        
        if (!Name2Key.containsKey(ToAvatarName.toLowerCase()))
        {
            // Send the Query
            Client.avatars.RequestAvatarNameSearch(ToAvatarName, UUID.Random());

            try {
				NameSearchEvent.waitOne(6000);
			} catch (InterruptedException e) {
				JLogger.warn(Utils.getExceptionStackTraceAsString(e));
			}
        }

        if (Name2Key.containsKey(ToAvatarName.toLowerCase()))
        {
            UUID id = Name2Key.get(ToAvatarName.toLowerCase());

            Client.self.InstantMessage(id, message);
            return "Instant Messaged " + id.toString() + " with message: " + message;
        }
        else
        {
            return "Name lookup for " + ToAvatarName + " failed";
        }
    }

    void Avatars_AvatarPickerReply(Object sender, AvatarPickerReplyEventArgs e)
    {
        for (Entry<UUID, String> kvp : e.getAvatars().entrySet())
        {
            if (kvp.getValue().toLowerCase().equals(ToAvatarName.toLowerCase()))
            {
                Name2Key.put(ToAvatarName.toLowerCase(), kvp.getKey());
                NameSearchEvent.set();
                return;
            }
        }
    }
}