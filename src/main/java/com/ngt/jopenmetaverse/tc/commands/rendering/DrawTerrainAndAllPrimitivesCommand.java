/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.rendering;

import com.ngt.jopenmetaverse.shared.sim.rendering.SceneObject;
import com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient.DefaultRenderingTerrainAndPrimitiveWithCameraWindow;

import com.ngt.jopenmetaverse.shared.types.UUID;

import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class DrawTerrainAndAllPrimitivesCommand extends Command
{
	public DrawTerrainAndAllPrimitivesCommand(TestClient testClient)
	{
		super(testClient);
		Name = "drawterrainandallprimitives";
		Description = "Primitives Rendering Example";
		Category = CommandCategory.Rendering;    
	}

	@Override
	public  String Execute(String[] args, UUID fromAgentID) throws Exception
	{
		final StringBuilder result = new StringBuilder();
		
//		DefaultRenderingPrimitiveWindow demo = new DefaultRenderingPrimitiveWindow(Client.network.getCurrentSim().Stats,  textureManager, renderPrimitives);
		DefaultRenderingTerrainAndPrimitiveWithCameraWindow demo 
			= new DefaultRenderingTerrainAndPrimitiveWithCameraWindow(Client);

        // set title, window size
        demo.window_title = "Current Terrain";
        demo.displayWidth = 640;
        demo.displayHeight = 480;
		
        demo.run();
		return result.toString();
	}
}
