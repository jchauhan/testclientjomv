/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.system;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.PlatformUtils;
import com.ngt.jopenmetaverse.tc.ClientManager;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class WaitForLoginCommand extends Command
{
    public WaitForLoginCommand(TestClient testClient)
    {
    	super(testClient);
        Name = "waitforlogin";
        Description = "Waits until all bots that are currently attempting to login have succeeded or failed";
        Category = CommandCategory.TestClient;
    }

    @Override
    public String Execute(String[] args, UUID fromAgentID)
    {
        while (ClientManager.getInstance().PendingLogins > 0)
        {
            PlatformUtils.sleep(1000);
            JLogger.info("Pending logins: " + ClientManager.getInstance().PendingLogins);
        }

        return "All pending logins have completed, currently tracking " + ClientManager.getInstance().Clients.size() + " bots";
    }
}