/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import java.io.UnsupportedEncodingException;
import java.util.Observable;

import com.ngt.jopenmetaverse.shared.protocol.AlertMessagePacket;
import com.ngt.jopenmetaverse.shared.protocol.Packet;
import com.ngt.jopenmetaverse.shared.protocol.PacketType;
import com.ngt.jopenmetaverse.shared.sim.Avatar;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.PacketReceivedEventArgs;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class FollowCommand extends Command
    {
        final float DISTANCE_BUFFER = 3.0f;
        //uint
        long targetLocalID = 0;

		public FollowCommand(TestClient testClient)
		{
			super(testClient);
			Name = "follow";
			Description = "Follow another avatar. Usage: follow [FirstName LastName]/off.";
            Category = CommandCategory.Movement;

            testClient.network.RegisterCallback(PacketType.AlertMessage, new EventObserver<PacketReceivedEventArgs>(){
    			@Override
    			public void handleEvent(Observable arg0, PacketReceivedEventArgs arg1) {
    				try {
    					AlertMessageHandler(arg0, (PacketReceivedEventArgs)arg1);
    				} catch (UnsupportedEncodingException e) {
    					JLogger.error("Error in AlertMessageHandler: " + Utils.getExceptionStackTraceAsString(e));
    				}
    			}}); 
		}

		@Override
        public String Execute(String[] args, UUID fromAgentID)
		{
            // Construct the target name from the passed arguments
			String target = "";
			for (int ct = 0; ct < args.length; ct++)
				target = target + args[ct] + " ";
			target = target.trim();

            if (target.length() == 0 || target.equals("off"))
            {
                Active = false;
                targetLocalID = 0;
                Client.self.AutoPilotCancel();
                return "Following is off";
            }
            else
            {
                if (Follow(target))
                    return "Following " + target;
                else
                    return "Unable to follow " + target + ".  Client may not be able to see that avatar.";
            }
		}

        boolean Follow(final String name)
        {
            synchronized (Client.network.Simulators)
            {
                for (int i = 0; i < Client.network.Simulators.size(); i++)
                {
                    Avatar target = Client.network.Simulators.get(i).ObjectsAvatars.Find(new Predicate<Avatar>()
                    		{
								public boolean match(Avatar t) {
		                            return t.getName().equals(name);
								}
                    		});

                    if (target != null)
                    {
                        targetLocalID = target.LocalID;
                        Active = true;
                        return true;
                    }
                }
            }

            if (Active)
            {
                Client.self.AutoPilotCancel();
                Active = false;
            }

            return false;
        }

        @Override
		public void Think()
		{
        	System.out.println(this.getClass().toString() + " Thinking");
            if (Active)
            {
                // Find the target position
            	synchronized (Client.network.Simulators)
                {
                    for (int i = 0; i < Client.network.Simulators.size(); i++)
                    {
                        Avatar targetAv;

                        if ((targetAv = Client.network.Simulators.get(i).ObjectsAvatars.get(targetLocalID)) !=null)
                        {
                            float distance = 0.0f;

                            //System.out.println(String.format("Target Position: %s, My Position: %s", targetAv.Position.toRawString(), Client.self.getSimPosition().toRawString()));
                            
                            if (Client.network.Simulators.get(i).equals(Client.network.getCurrentSim()))
                            {
                                distance = Vector3.distance(targetAv.Position, Client.self.getSimPosition());
                            }
                            else
                            {
                                // FIXME: Calculate global distances
                            }
//                            System.out.println("Distance with target avatar: " + distance + " Sim handle: " + 
//                            Utils.bytesToHexDebugString(Utils.int64ToBytes(Client.network.Simulators.get(i).Handle.longValue()), ""));
                            if (distance > DISTANCE_BUFFER)
                            {
                            	//uint
                                long[] regionXY = new long[2];
                                Utils.longToUInts(Client.network.Simulators.get(i).Handle.longValue(), regionXY);
                               // System.out.println(String.format("Moving to <%d, %d>", regionXY[0], regionXY[1]));

                                //FIXME need to uncomment following
                                double xTarget = (double)targetAv.Position.X + (double)regionXY[0];
                                double yTarget = (double)targetAv.Position.Y + (double)regionXY[1];
//                                double xTarget = (double)targetAv.Position.X;
//                                double yTarget = (double)targetAv.Position.Y;
                                double zTarget = targetAv.Position.Z - 2f;

                                JLogger.debug(String.format("[Autopilot] %f meters away from the target, starting autopilot to <%f,%f,%f>",
                                    distance, xTarget, yTarget, zTarget));

                                //System.out.println(Utils.bytesToHexDebugString(Client.self.createGenericMessagePacket(xTarget, yTarget, zTarget).ToBytes(), ""));
                                //System.out.println(String.format("SessionID %s --> AgentID %s" , Client.self.getSessionID().toString(), Client.self.getAgentID().toString()));
                                
                                Client.self.AutoPilot(xTarget, yTarget, zTarget);
                                System.out.println(String.format("Moving to <%f, %f, %f>", xTarget, yTarget, zTarget));
                            }
                            else
                            {
                                // We are in range of the target and moving, stop moving
                                Client.self.AutoPilotCancel();
                                System.out.println("Not Moving....");
                            }
                        }
                    }
                }
            }

			super.Think();
		}

        private void AlertMessageHandler(Object sender, PacketReceivedEventArgs e) throws UnsupportedEncodingException
        {
            Packet packet = e.getPacket();
            
            AlertMessagePacket alert = (AlertMessagePacket)packet;
            String message = Utils.bytesToString(alert.AlertData.Message);

            //System.out.println("Got Packet: " + message);
            
            if (message.contains("Autopilot cancel"))
            {
                JLogger.debug("FollowCommand: " + message);
            }
        }
    }