/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.communication;


import java.util.Observable;

import com.ngt.jopenmetaverse.shared.sim.AgentManager.ChatType;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.ManualResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.am.GroupChatJoinedEventArgs;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class ImGroupCommand extends Command
    {
        UUID ToGroupID = UUID.Zero;
        ManualResetEvent WaitForSessionStart = new ManualResetEvent(false);
        public ImGroupCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "imgroup";
            Description = "Send an instant message to a group. Usage: imgroup [group_uuid] [message]";
            Category = CommandCategory.Communication;
        }

        @Override
        public  String Execute(String[] args, UUID fromAgentID)
        {
        	try{
            if (args.length < 2)
                return "Usage: imgroup [group_uuid] [message]";

            ToGroupID = new UUID(args[0]);
            if (ToGroupID != null)
            {
            	System.out.println("Group ID: " + ToGroupID.toString());
                String message = "";
                for (int ct = 1; ct < args.length; ct++)
                    message += args[ct] + " ";

//                message = message.TrimEnd();
                message = message.trim();

                if (message.length() > 1023)
                {
                    message = message.substring(0, 1023);
                    System.out.println("Message truncated at 1024 characters");
                }
                
                EventObserver<GroupChatJoinedEventArgs> eventObserver = new EventObserver<GroupChatJoinedEventArgs>()
                        {
        					@Override
        					public void handleEvent(Observable o, GroupChatJoinedEventArgs arg) {
        						Self_GroupChatJoined(o, arg);
        					}
                        }; 

                Client.self.registerGroupChatJoined(eventObserver);

                if (!Client.self.GroupChatSessions.containsKey(ToGroupID))
                {
                    WaitForSessionStart.reset();
                    Client.self.RequestJoinGroupChat(ToGroupID);
                }
                else
                {
                    WaitForSessionStart.set();
                }
                
                try{
                if (WaitForSessionStart.waitOne(20000))
                {
                    Client.self.InstantMessageGroup(ToGroupID, message);
                }
                else
                {
                    return "Timeout waiting for group session start";
                }
                }
                catch(InterruptedException e)
                { return "Got Interrupted while waiting for group session start";}

                Client.self.unregisterGroupChatJoined(eventObserver);
                return "Instant Messaged group " + ToGroupID.toString() + " with message: " + message;
            }
            else
            {
                return "failed to instant message group";
            }
        	}
        	catch(Exception e)
        	{
        		JLogger.warn(Utils.getExceptionStackTraceAsString(e));
        		return "Unknown Error...";
        	}
        }

        void Self_GroupChatJoined(Object sender, GroupChatJoinedEventArgs e)
        {
            if (e.getSuccess())
            {
                System.out.println(String.format("Joined %s Group Chat Success!", e.getSessionName()));
                WaitForSessionStart.set();
            }
            else
            {
                System.out.println("Join Group Chat failed :(");
            }
        }

       
    }