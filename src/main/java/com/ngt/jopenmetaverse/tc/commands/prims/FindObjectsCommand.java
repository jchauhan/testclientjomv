/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.prims;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.sim.events.AutoResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.om.ObjectPropertiesEventArgs;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


public class FindObjectsCommand extends Command
{
    Map<UUID, Primitive> PrimsWaiting = new HashMap<UUID, Primitive>();
    AutoResetEvent AllPropertiesReceived = new AutoResetEvent(false);

    public FindObjectsCommand(TestClient testClient)
    {
    	super(testClient);
        testClient.objects.registerOnObjectProperties(new EventObserver<ObjectPropertiesEventArgs>()
        		{
					@Override
					public void handleEvent(Observable o,
							ObjectPropertiesEventArgs arg) {
						Objects_OnObjectProperties(o, arg);
					}
        		});
        		
        Name = "findobjects";
        Description = "Finds all objects, which name contains search-string. Default Radius is 10" +
            "Usage: findobjects [radius] <search-string>";
        Category = CommandCategory.Objects;
    }

    @Override
    public String Execute(String[] args, UUID fromAgentID) throws InterruptedException
    {
        // *** parse arguments ***
        if ((args.length < 1) || (args.length > 2))
            return "Usage: findobjects [radius] <search-string>";
        int i =0;
        final float[] radius = new float[] {10};
        if(Utils.tryParseFloat(args[i], radius))
        {
        	i+=1;
        }
        String searchString = (args.length > i) ? args[i] : "";

        // *** get current location ***
        final Vector3 location = Client.self.getSimPosition();

        System.out.println("My Location: " + location.toString());
        
        // *** find all objects in radius ***
        List<Primitive> prims = Client.network.getCurrentSim().ObjectsPrimitives.FindAll(
            new Predicate<Primitive>()
            {
				public boolean match(Primitive prim) {
	                Vector3 pos = prim.Position;
	                if(prim.Position == null)
	                	System.out.println("Primitive Position was found to be null for : " + prim.LocalID);
	                return (
	                		(Vector3.notEquals(pos, Vector3.Zero)) 
	                		&& (Vector3.distance(pos, location) < radius[0]));
				}
            }
        );

        // *** request properties of these objects ***
        boolean complete = RequestObjectProperties(prims, 25);

        for (Primitive p : prims)
        {
            String name = p.Properties != null ? p.Properties.Name : null;
            if ( !Utils.isNullOrEmpty(name) && (Utils.isNullOrEmpty(searchString) || name.contains(searchString)) )
                System.out.println(String.format("Object '%s': %s, %d [Pos: %s Rot %s]", name, p.ID.toString(), 
                		p.LocalID, p.Position.toString(), p.Rotation.toString()));
        }

        if (!complete)
        {
            System.out.println("Warning: Unable to retrieve full properties for:");
            for (UUID uuid : PrimsWaiting.keySet())
                System.out.println(uuid);
        }

        return "Done searching";
    }

    private boolean RequestObjectProperties(List<Primitive> objects, int msPerRequest) throws InterruptedException
    {
        // Create an array of the local IDs of all the prims we are requesting properties for
        long[] localids = new long[objects.size()];

        synchronized (PrimsWaiting)
        {
            PrimsWaiting.clear();

            for (int i = 0; i < objects.size(); ++i)
            {
                localids[i] = objects.get(i).LocalID;
                PrimsWaiting.put(objects.get(i).ID, objects.get(i));
            }
        }

        Client.objects.SelectObjects(Client.network.getCurrentSim(), localids);

        return AllPropertiesReceived.waitOne(2000 + msPerRequest * objects.size());
    }

    void Objects_OnObjectProperties(Object sender, ObjectPropertiesEventArgs e)
    {
    	synchronized (PrimsWaiting)
        {
            Primitive prim;
            if ( (prim = PrimsWaiting.get(e.getProperties().ObjectID))!=null)
            {
                prim.Properties = e.getProperties();
            }
            PrimsWaiting.remove(e.getProperties().ObjectID);

            if (PrimsWaiting.size() == 0)
                AllPropertiesReceived.set();
        }
    }
}
