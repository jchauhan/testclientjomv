/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.appearance;

import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.AppearanceManager.AvatarTextureIndex;
import com.ngt.jopenmetaverse.shared.sim.Avatar;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class AvatarInfoCommand extends Command
    {
        public AvatarInfoCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "avatarinfo";
            Description = "Print out information on a nearby avatar. Usage: avatarinfo [firstname] [lastname]";
            Category = CommandCategory.Appearance;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID)
        {
            if (args.length != 2)
                return "Usage: avatarinfo [firstname] [lastname]";

            final String targetName = String.format("%s %s", args[0], args[1]);

            Avatar foundAv = Client.network.getCurrentSim().ObjectsAvatars.Find(new Predicate<Avatar>()
            		{
						public boolean match(Avatar avatar) {
							return (avatar.getName().equals(targetName));
						}            	
            		});

            if (foundAv != null)
            {
                StringBuilder output = new StringBuilder();

                output.append(String.format("%s (%s)\n", targetName, foundAv.ID));

                for (int i = 0; i < foundAv.Textures.FaceTextures.length; i++)
                {
                    if (foundAv.Textures.FaceTextures[i] != null)
                    {
                        TextureEntryFace face = foundAv.Textures.FaceTextures[i];
                        AvatarTextureIndex type = AvatarTextureIndex.get(i);

                        output.append(String.format("%s: %s\n", type, face.getTextureID()));
                    }
                }

                return output.toString();
            }
            else
            {
                return "No nearby avatar with the name " + targetName;
            }
        }
    }