/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.communication;

import com.ngt.jopenmetaverse.shared.sim.AgentManager.ChatType;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class SayCommand extends  Command
{
    public SayCommand(TestClient testClient)
    {
    	super(testClient);
        Name = "say";
        Description = "Say something.  (usage: say (optional channel) whatever)";
        Category = CommandCategory.Communication;
    }

    @Override
    public String Execute(String[] args, UUID fromAgentID)
    {
        int channel = 0;
        int startIndex = 0;

        if (args.length < 1)
        {
            return "usage: say (optional channel) whatever";
        }
        else if (args.length > 1)
        {
        	try
        	{
            channel = Integer.parseInt(args[0]);
        	}
        	catch(NumberFormatException e) {startIndex = 1;}
        }

        String message = "";

        for (int i = startIndex; i < args.length; i++)
        {
            // Append a space before the next arg
            if( i > 0 )
                message += " ";
            message += args[i];
        }

        Client.self.Chat(message, channel, ChatType.Normal);

        return "Said " + message.toString();
    }
}
