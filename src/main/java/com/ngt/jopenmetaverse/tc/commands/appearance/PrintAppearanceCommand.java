/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.appearance;

import java.util.ArrayList;
import com.ngt.jopenmetaverse.shared.protocol.AgentSetAppearancePacket;
import com.ngt.jopenmetaverse.shared.protocol.AvatarAppearancePacket;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntry;
import com.ngt.jopenmetaverse.shared.sim.Avatar;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryItem;
import com.ngt.jopenmetaverse.shared.structureddata.llsd.JsonLLSDOSDParser;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


public class PrintAppearanceCommand extends Command
    {
			//uint
	        long SerialNum = 2;

	        public PrintAppearanceCommand(TestClient testClient)
	        {
	        	super(testClient);
	            Name = "printappearance";
	            Description = "Print the appearance of a nearby avatar. Usage: printappearance [firstname] [lastname]";
	            Category = CommandCategory.Appearance;
	        }
	        
	        @Override
	        public String Execute(String[] args, UUID fromAgentID) throws Exception
	        {
	        	  if (args.length != 2)
	                  return "Usage: printappearance [firstname] [lastname]";

	        	  StringBuilder sb = new StringBuilder();
	              String targetName = String.format("%s %s", args[0], args[1]);
	              sb.append(targetName + "\n");
	              final String searchString = targetName;
	              Avatar foundAv = Client.network.getCurrentSim().ObjectsAvatars.Find(new Predicate<Avatar>()
	              		{
	  						public boolean match(Avatar avatar) {
	  							return (avatar.getName().equals(searchString));
	  						}            	
	              		});
	            
	            
	            if (foundAv != null)
	            {
	                UUID target = foundAv.ID;
	                targetName += String.format(" (%s)", target);

	                if(target.equals(Client.self.getAgentID()))
	                {
	                	TextureEntry textureEntry = Client.appearance.MyTextures; 
	                    byte[] serializedTextureEntry = JsonLLSDOSDParser.SerializeLLSDJsonBytes(textureEntry.GetOSD());
	                    sb.append(Utils.bytesToString(serializedTextureEntry));	 
	                    return sb.toString();
	                }
	                else
	                if (Client.Appearances.containsKey(target))
	                {
	                    //region AvatarAppearance to AgentSetAppearance
	                	
	                    AvatarAppearancePacket appearance = Client.Appearances.get(target);
	                    
	                    TextureEntry textureEntry = new TextureEntry(appearance.ObjectData.TextureEntry, 0, appearance.ObjectData.TextureEntry.length);
	                    sb.append(Utils.bytesToHexDebugString(appearance.ObjectData.TextureEntry, ""));	                    

	                    byte[] serializedTextureEntry = JsonLLSDOSDParser.SerializeLLSDJsonBytes(textureEntry.GetOSD());
	                    sb.append(Utils.bytesToString(serializedTextureEntry));	                    
	                    
	                 
	                    //endregion AvatarAppearance to AgentSetAppearance

	                    return sb.toString();
	                }
	                else
	                {
	                    return "Don't know the appearance of avatar " + targetName;
	                }
	            }
	            else
	            {
	                return "Couldn't find avatar " + targetName;
	            }
	        }
	 }