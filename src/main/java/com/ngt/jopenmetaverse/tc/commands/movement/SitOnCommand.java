/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class SitOnCommand extends Command
    {
        public SitOnCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "siton";
            Description = "Attempt to sit on a particular prim, with specified UUID";
            Category = CommandCategory.Movement;
        }

        @Override
		public String Execute(String[] args, UUID fromAgentID)
        {
            if (args.length != 1)
                return "Usage: siton UUID";

            final UUID[] target = new UUID[1];

            if (Utils.tryParseUUID(args[0], target))
            {
                Primitive targetPrim = Client.network.getCurrentSim().ObjectsPrimitives.Find(
                    new Predicate<Primitive>()
                    {

					public boolean match(Primitive prim) {
	                        return prim.ID.equals(target[0]);
					}
                    }
                );

                if (targetPrim != null)
                {
                    Client.self.RequestSit(targetPrim.ID, Vector3.Zero);
                    Client.self.Sit();
                    return "Requested to sit on prim " + targetPrim.ID.toString() +
                        " (" + targetPrim.LocalID + ")";
                }
            }

            return "Couldn't find a prim to sit on with UUID " + args[0];
        }
    }