/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;

public abstract class Command implements Comparable<Command>
{
	public String Name;
	public String Description;
    public CommandCategory Category;

	public TestClient Client;

    public Command(TestClient testClient)
	{
    	Client = testClient;
	}
	
	public abstract String Execute(String[] args, UUID fromAgentID) throws Exception;

	/// <summary>
	/// When set to true, think will be called.
	/// </summary>
	public boolean Active;

	/// <summary>
	/// Called twice per second, when Command.Active is set to true.
	/// </summary>
	//virtual??
	public  void Think()
	{
	}

	public int compareTo(Command obj) {
            Command c2 = (Command)obj;
            return Category.compareTo(c2.Category);
    }
}
