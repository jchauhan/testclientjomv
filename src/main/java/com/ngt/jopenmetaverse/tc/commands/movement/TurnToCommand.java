/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class TurnToCommand extends Command
    {
        public TurnToCommand(TestClient client)
        {
        	super(client);
            Name = "turnto";
            Description = "Turns the avatar looking to a specified point. Usage: turnto x y z";
            Category = CommandCategory.Movement;
        }
        @Override
		public String Execute(String[] args, UUID fromAgentID)
        {
            if (args.length != 3)
                return "Usage: turnto x y z";
            double[][] xyz = new double[3][1];
            if (!Utils.tryParseDouble(args[0], xyz[0]) ||
                !Utils.tryParseDouble(args[1], xyz[1]) ||
                !Utils.tryParseDouble(args[2], xyz[2]))
            {
                return "Usage: turnto x y z";
            }

            Vector3 newDirection = new Vector3();
            newDirection.X = (float)xyz[0][0];
            newDirection.Y = (float)xyz[1][0];
            newDirection.Z = (float)xyz[2][0];
            Client.self.Movement.TurnToward(newDirection);
            Client.self.Movement.SendUpdate(false);
            return "Turned to ";
        }
    }