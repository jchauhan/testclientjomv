/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import com.ngt.jopenmetaverse.shared.sim.AgentManager;
import com.ngt.jopenmetaverse.shared.sim.AgentManager.AgentFlags;
import com.ngt.jopenmetaverse.shared.sim.AgentManager.AgentState;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.PlatformUtils;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class ForwardCommand extends Command
    {
        public ForwardCommand(TestClient client)
        {
        	super(client);
            Name = "forward";
            Description = "Sends the move forward command to the server for a single packet or a given number of seconds. Usage: forward [seconds]";
            Category = CommandCategory.Movement;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID)
        {
            if (args.length > 1)
                return "Usage: forward [seconds]";

            if (args.length == 0)
            {
                Client.self.Movement.SendManualUpdate(AgentManager.ControlFlags.AGENT_CONTROL_AT_POS, Client.self.Movement.Camera.getPosition(),
                    Client.self.Movement.Camera.getAtAxis(), Client.self.Movement.Camera.getLeftAxis(), Client.self.Movement.Camera.getUpAxis(),
                    Client.self.Movement.BodyRotation, Client.self.Movement.HeadRotation, Client.self.Movement.Camera.Far, AgentFlags.None,
                    AgentState.None, true);
            }
            else
            {
                // Parse the number of seconds
            	int[] da = new int[1];
                int duration;
                if (!Utils.tryParseInt(args[0], da))
                {
                    return "Usage: forward [seconds]";
                }
                // Convert to milliseconds
                duration = da[0];
                duration *= 1000;
               
                long start = Utils.getUnixTime();

                Client.self.Movement.setAtPos(true);

                while (Utils.getUnixTime() - start < duration)
                {
                	System.out.println(String.format("%d %d %d", Utils.getUnixTime(), start, duration));
                    // The movement timer will do this automatically, but we do it here as an example
                    // and to make sure updates are being sent out fast enough
                    Client.self.Movement.SendUpdate(false);
                    PlatformUtils.sleep(100);
                }

                Client.self.Movement.setAtPos(false);
            }

            return "Moved forward";
        }
    }