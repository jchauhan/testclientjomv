/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.agent;

import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;

import com.ngt.jopenmetaverse.shared.sim.Animations;
import com.ngt.jopenmetaverse.shared.types.Action;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


 public class PlayAnimationCommand extends Command
    {        
        private Map<UUID, String> m_BuiltInAnimations;
        public PlayAnimationCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "play";
            Description = "Attempts to play an animation";
            Category = CommandCategory.Appearance;                        
        }

        private String Usage()
        {
            String usage = "Usage:\n" +
                "\tplay list - list the built in animations\n" +
                "\tplay show - show any currently playing animations\n" +
                "\tplay UUID - play an animation asset\n" +
                "\tplay ANIMATION - where ANIMATION is one of the values returned from \"play list\"\n";
            return usage;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID) throws IllegalArgumentException, IllegalAccessException
        {            
        	m_BuiltInAnimations = new HashMap<UUID, String>(Animations.toMap());
            final StringBuilder result = new StringBuilder();
            if (args.length != 1)
                return Usage();

            UUID[] animationID = new UUID[1];
            String arg = args[0].trim();
            
            
            if ((Utils.tryParseUUID(args[0], animationID)))
            {
                Client.self.AnimationStart(animationID[0], true);
            }
            else if (arg.toLowerCase().equals("list"))
            {
                for (String key : m_BuiltInAnimations.values())
                {
                    result.append(key + "\n");
                }
            }
            else if (arg.toLowerCase().equals("show"))
            {
                Client.self.SignaledAnimations.foreach( new Action<Entry<UUID, Integer>>()
                		{
							public void execute(Entry<UUID, Integer> kvp) {
		                            if (m_BuiltInAnimations.containsKey(kvp.getKey()))
		                            {
		                                result.append(String.format("The %s System Animation is being played, sequence is %s", 
		                                		m_BuiltInAnimations.get(kvp.getKey()), kvp.getValue()));
		                            }
		                            else
		                            {
		                            	result.append(String.format("The %s Asset Animation is being played, sequence is %s", kvp.getKey(), kvp.getValue()));
		                            }
							}
                		}
                );                                
            }
            else if (m_BuiltInAnimations.containsValue(args[0].trim().toUpperCase()))
            {
                for (Entry<UUID, String> kvp : m_BuiltInAnimations.entrySet())
                {
                    if (kvp.getValue().equals(arg.toUpperCase()))
                    {
                        Client.self.AnimationStart(kvp.getKey(), true);
                        break;
                    }
                }
            }
            else
            {
                return Usage();
            }

            return result.toString();
        }
    }