/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class CommandFactory {
	private  SortedMap<CommandCategory, List<Command>> commandCategoryMap =  new TreeMap<CommandCategory, List<Command>>();
	private  Map<String, Command> commandNameMap =  new HashMap<String, Command>();
	
	public  void registerCommand(Command cmd)
	{
		if(!commandCategoryMap.containsKey(cmd.Category))
			commandCategoryMap.put(cmd.Category, new ArrayList<Command>());
		
		commandCategoryMap.get(cmd.Category).add(cmd);
		commandNameMap.put(cmd.Name.toLowerCase(), cmd);
	}
	
	public  CommandCategory[] getCommandCategoryKeys()
	{
		return commandCategoryMap.keySet().toArray(new CommandCategory[0]);
	}
	
	public  List<Command> get(CommandCategory c)
	{
		return commandCategoryMap.get(c);
	}
	
	public  Command get(String name)
	{
		return commandNameMap.get(name);
	}
	
	public  boolean contains(String name)
	{
		return commandNameMap.containsKey(name);
	}
	
	public  Collection<Command> values()
	{
		return commandNameMap.values();
	}
}
