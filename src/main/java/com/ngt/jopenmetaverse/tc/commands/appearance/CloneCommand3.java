/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.appearance;

import java.util.ArrayList;
import com.ngt.jopenmetaverse.shared.protocol.AgentSetAppearancePacket;
import com.ngt.jopenmetaverse.shared.protocol.AvatarAppearancePacket;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntry;
import com.ngt.jopenmetaverse.shared.sim.Avatar;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryItem;
import com.ngt.jopenmetaverse.shared.structureddata.llsd.JsonLLSDOSDParser;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


public class CloneCommand3 extends Command
{
	//uint
	long SerialNum = 2;

	public CloneCommand3(TestClient testClient)
	{
		super(testClient);
		Name = "clone3";
		Description = "Clone the appearance of a nearby avatar. Usage: clone2 <firstname> <lastname> [facenumber1] [facenumber2] ..";
		Category = CommandCategory.Appearance;
	}

	@Override
	public String Execute(String[] args, UUID fromAgentID) throws Exception
	{
		if (args.length < 2)
			return "Usage: clone2 <firstname> <lastname> [facenumber1] [facenumber2] ..";

		String targetName = String.format("%s %s", args[0], args[1]);

		int[] facenums = new int[args.length - 2];

		for(int i = 2; i < args.length; i++)
		{
			facenums[i-2] = Integer.parseInt(args[i]);
		}

		final String searchString = targetName;
		Avatar foundAv = Client.network.getCurrentSim().ObjectsAvatars.Find(new Predicate<Avatar>()
				{
			public boolean match(Avatar avatar) {
				return (avatar.getName().equals(searchString));
			}            	
				});


		if (foundAv != null)
		{
			UUID target = foundAv.ID;
			targetName += String.format(" (%s)", target);

			Avatar me;
			me = Client.network.getCurrentSim().ObjectsAvatars.get(Client.self.getLocalID());

			if (me != null && Client.Appearances.containsKey(target))
			{
				//region AvatarAppearance to AgentSetAppearance

				AvatarAppearancePacket appearance = Client.Appearances.get(target);

				AgentSetAppearancePacket set = new AgentSetAppearancePacket();
				set.AgentData.AgentID = Client.self.getAgentID();
				set.AgentData.SessionID = Client.self.getSessionID();
				set.AgentData.SerialNum = SerialNum++;
				set.AgentData.Size = new Vector3(2f, 2f, 2f); // HACK

				set.WearableData = new AgentSetAppearancePacket.WearableDataBlock[0];
				set.VisualParam = new AgentSetAppearancePacket.VisualParamBlock[me.VisualParameters.length];

				for (int i = 0; i < me.VisualParameters.length; i++)
				{
					set.VisualParam[i] = new AgentSetAppearancePacket.VisualParamBlock();
					set.VisualParam[i].ParamValue = me.VisualParameters[i];
				}

				TextureEntry targetTextureEntry = new TextureEntry(appearance.ObjectData.TextureEntry, 0, appearance.ObjectData.TextureEntry.length);

				for(int i =0; i < facenums.length; i++)
				{	
					UUID imageid = null;
					for(int j = 0; j< targetTextureEntry.FaceTextures.length; j++)
					{
						if(targetTextureEntry.FaceTextures[j] != null && facenums[i] == j)
						{
							imageid = targetTextureEntry.FaceTextures[j].getTextureID();
							break;
						}
					}
					if(imageid != null)
					{
						boolean found = false;
						for(int j = 0; j< me.Textures.FaceTextures.length; j++)
						{
							if(me.Textures.FaceTextures[j] != null && facenums[i] == j)
							{
								me.Textures.FaceTextures[j].setTextureID(imageid);
								System.out.println("Setting imageID " + imageid + " on face num" + facenums[i]);
								found = true;
								break;
							}
						}
						if(!found)
							System.out.println("Could not find face num in src " + facenums[i]);
					}
					else
					{
						System.out.println("Could not find face num in target " + facenums[i]);
					}

					System.out.println("Setting facenum "+ facenums[i]);
					System.out.println("Target Texture ID" + targetTextureEntry.FaceTextures[facenums[i]].getTextureID());
					me.Textures.FaceTextures[facenums[i]].setTextureID(targetTextureEntry.FaceTextures[facenums[i]].getTextureID());
				}

				set.ObjectData.TextureEntry = me.Textures.GetBytes();

				//endregion AvatarAppearance to AgentSetAppearance

				// Detach everything we are currently wearing
				Client.appearance.AddAttachments(new ArrayList<InventoryItem>(), true);

				byte[] serializedTextureEntry = JsonLLSDOSDParser.SerializeLLSDJsonBytes(me.Textures.GetOSD());
				System.out.println(Utils.bytesToString(serializedTextureEntry));

				// Send the new appearance packet
				Client.network.SendPacket(set);

				return "Cloned " + targetName;
			}
			else
			{
				return "Don't know the appearance of avatar " + targetName;
			}
		}
		else
		{
			return "Couldn't find avatar " + targetName;
		}
	}
}