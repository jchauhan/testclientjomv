/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.rendering;

import java.util.ArrayList;
import java.util.List;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.ColorVertex;
import com.ngt.jopenmetaverse.shared.sim.rendering.FaceData;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderPrimitive;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderTerrain;
import com.ngt.jopenmetaverse.shared.sim.rendering.SceneObject;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureLoadItem;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureManagerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.DetailLevel;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.Face;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.InterleavedVBO;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.MeshmerizerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primitive.PrimitiveMesher;
import com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient.DefaultRenderingTerrainAndPrimitiveWithCameraWindowBak;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainHelper;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainSplat;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.ProfileCurve;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class DrawTerrainAndAllPrimitivesCommandBak extends Command
{
	TextureManagerR textureManager  = null;
	public DrawTerrainAndAllPrimitivesCommandBak(TestClient testClient)
	{
		super(testClient);
		Name = "drawterrainandallprimitives";
		Description = "Primitives Rendering Example";
		Category = CommandCategory.Rendering;    
		textureManager = new TextureManagerR(Client);
	}

	@Override
	public  String Execute(String[] args, UUID fromAgentID) throws Exception
	{
		final StringBuilder result = new StringBuilder();
		
		Simulator sim = Client.network.getCurrentSim();
		
		List<RenderPrimitive> renderPrimitives = new ArrayList<RenderPrimitive>();
		
		PrimitiveMesher primMesher = new PrimitiveMesher(Client); 
//		List<Primitive> mainPrims = sim.ObjectsPrimitives.FindAll(new Predicate<Primitive>()
//		{
//			public boolean match(Primitive p) 
//			{
//				return p.ParentID == 0;
//			}	
//		});

		List<Primitive> mainPrims = sim.ObjectsPrimitives.getChildren(0L);
		
		System.out.println("Size of Main prims: " + mainPrims.size());
		
		for (Primitive mainPrim : mainPrims)
		{			
			if(mainPrim.PrimData == null)
				System.out.println("PrimData is null");
			System.out.println(mainPrim.PrimData.profileCurve + "Prim Data: " + ProfileCurve.get(mainPrim.PrimData.profileCurve));
			if(ProfileCurve.get(mainPrim.PrimData.profileCurve) != null)
				System.out.println("\t\t" + mainPrim.LocalID + " " + mainPrim.getType() + " " + mainPrim.PrimData.PCode);
			else
			{
				//TODO why are getting prim with type unknown 17
				System.out.println("Could Not get Prim type for : " +  mainPrim.ID.toString());
				continue;
			}
			
			RenderPrimitive rmainprim = UpdatePrimBlocking(primMesher, mainPrim);
			
			if(rmainprim.Faces == null)
			{
				//Why are we getting this
				System.out.println("Could Not generate Mesh for Prim: " +  rmainprim.getBasePrim().ID.toString());
				continue;
			}
			
			renderPrimitives.add(rmainprim);
			
			final long rootPrimId = mainPrim.LocalID;
			
			System.out.println("Primitive Info " + mainPrim.LocalID + " " + mainPrim.getType() + " " + mainPrim.PrimData.PCode);
			
			//TODO only for debugging
			//printTextures(rmainprim, mainPrim, "\t");
			
//			List<Primitive> childPrims = sim.ObjectsPrimitives.FindAll(new Predicate<Primitive>()
//					{
//				public boolean match(Primitive t) {
//					return t.ParentID == rootPrimId;
//				}
//					});

			List<Primitive> childPrims = sim.ObjectsPrimitives.getChildren(rootPrimId);
			System.out.println("Size of Child prims: " + childPrims.size());
			
			for(Primitive subPrim: childPrims)
			{   
				if(subPrim.PrimData == null)
					System.out.println("PrimData is null");
				System.out.println(subPrim.PrimData.profileCurve + "Prim Data: " + ProfileCurve.get(subPrim.PrimData.profileCurve));
				if(ProfileCurve.get(subPrim.PrimData.profileCurve) != null)
					System.out.println("\t\t Primitive Info" + subPrim.LocalID + " " + subPrim.getType() + " " + subPrim.PrimData.PCode);
				else
				{
					//TODO why are getting prim with type unknown 17
					System.out.println("Could Not get Prim type for : " +  subPrim.ID.toString());
					continue;
				}
				RenderPrimitive rsubprim = UpdatePrimBlocking(primMesher, subPrim);
				rsubprim.setParentSceneObject(rmainprim);
				if(rsubprim.Faces == null)
				{
					//Why are we getting this
					System.out.println("Could Not generate Mesh for Prim: " +  rsubprim.getBasePrim().ID.toString());
					continue;
				}
				renderPrimitives.add(rsubprim);
				
				//TODO only for debugging
				printTextures(rsubprim, subPrim, "\t\t\t");
			}
		}

		updateTerrain();
		RenderTerrain renderTerrain = new RenderTerrain(Client, newVertexVBO, newIndices, terrainImage);

//		DefaultRenderingPrimitiveWindow demo = new DefaultRenderingPrimitiveWindow(Client.network.getCurrentSim().Stats,  textureManager, renderPrimitives);
		DefaultRenderingTerrainAndPrimitiveWithCameraWindowBak demo = new DefaultRenderingTerrainAndPrimitiveWithCameraWindowBak(Client,  textureManager, renderPrimitives, renderTerrain);

        // set title, window size
        demo.window_title = "Current Terrain";
        demo.displayWidth = 640;
        demo.displayHeight = 480;
		
        demo.run();
		return result.toString();
	}

	public RenderPrimitive UpdatePrimBlocking(PrimitiveMesher primMesher, Primitive prim) throws Exception
	{

		RenderPrimitive rPrim = new RenderPrimitive();
		rPrim.setBasePrim(prim);
		rPrim.Meshed = false;
		rPrim.BoundingVolume = new BoundingVolume();
		rPrim.BoundingVolume.FromScale(prim.Scale);         
		primMesher.MeshPrim(rPrim, true, DetailLevel.High, DetailLevel.High, DetailLevel.Highest);
		return rPrim;
	}

	void printTextures(RenderPrimitive rprim, Primitive prim, String indent) throws Exception
	{
		for(int i =0;  i < rprim.Faces.size(); i ++)
		{
			TextureEntryFace tef = prim.Textures.GetFace(i);
			System.out.println(String.format(indent + "Face Index: %d, Texture ID: %s", i, tef.getTextureID()));
			if(!Client.assets.Cache.hasAsset(tef.getTextureID()))
			{
				System.out.println(indent + "Downloading textures ... ");
				Client.assets.RequestImage(tef.getTextureID(), getTextureDownloadCallback(tef.getTextureID().toString()));
			}
			textureManager.requestDownloadTexture(new TextureLoadItem((FaceData)rprim.Faces.get(i).UserData, 
					prim, prim.Textures.GetFace(i)));
		}
	}
	
	public MethodDelegate<Void, TextureDownloadCallbackArgs>  getTextureDownloadCallback(final String filename)
	{
		return new MethodDelegate<Void, TextureDownloadCallbackArgs>()
		{
			public Void execute(TextureDownloadCallbackArgs e) 
			{
				switch (e.getState())
				{
				case Finished:
					Client.assets.Cache.saveAssetToCache(filename, e.getAssetTexture().AssetData);
					break;

				case Aborted:
				case NotFound:
				case Timeout:
					break;
				}
				return null;
			}

		};
	}
	
	float[][] heightMap = new float[256][256];
	short[] newIndices;
	InterleavedVBO newVertexVBO;
	IBitmap terrainImage; 

	
	public void updateTerrain() throws Exception
	{		
		Simulator sim = Client.network.getCurrentSim();
		heightMap = TerrainHelper.createHeightTable(sim.Terrain, heightMap);
		MeshmerizerR renderer = new MeshmerizerR();
		Face terrainFace = renderer.TerrainMesh(heightMap, 0f, 255f, 0f, 255f);
		ColorVertex[] terrainVertices = TerrainHelper.genColorVertices(terrainFace);
		newIndices = terrainFace.toIndicesArray();
		byte[] data = ColorVertex.toBytes(terrainVertices);
		newVertexVBO = new InterleavedVBO(data, ColorVertex.Size, 3, 3, 
				2, 0, 12, 24);
		
//		Simulator sim = client.network.getCurrentSim();
		UUID[] textureIds = new UUID[] { sim.TerrainDetail0, sim.TerrainDetail1, sim.TerrainDetail2, sim.TerrainDetail3 };
		float[] startHeights = new float[] { sim.TerrainStartHeight00, sim.TerrainStartHeight01, sim.TerrainStartHeight10, sim.TerrainStartHeight11 };
		float[] heightRanges = new float[] { sim.TerrainHeightRange00, sim.TerrainHeightRange01, sim.TerrainHeightRange10, sim.TerrainHeightRange11 };
		terrainImage = TerrainSplat.Splat(Client, heightMap, textureIds, startHeights, heightRanges);

		System.out.println(String.format("Image splated Width %d, Height %d", terrainImage.getWidth(), terrainImage.getHeight()));
	}	
	
    /// <summary>
    /// Finds the closest distance between the given pos and an object
    /// (Assumes that the object is a box slightly)
    /// </summary>
    /// <param name="vector3"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    private float FindClosestDistanceSquared(Vector3 calcPos, SceneObject p)
    {
//        if (p.BoundingVolume == null
//            || !RenderSettings.HeavierDistanceChecking
//            || p.BoundingVolume.ScaledR < 10f
//            )
            return Vector3.distanceSquared(calcPos, p.RenderPosition);

//        Vector3 posToCheckFrom = Vector3.Zero;
//        //Get the bounding boxes for this prim
//        Vector3 boundingBoxMin = p.RenderPosition + p.BoundingVolume.ScaledMin;
//        Vector3 boundingBoxMax = p.RenderPosition + p.BoundingVolume.ScaledMax;
//        posToCheckFrom.X = (calcPos.X < boundingBoxMin.X) ? boundingBoxMin.X : (calcPos.X > boundingBoxMax.X) ? boundingBoxMax.X : calcPos.X;
//        posToCheckFrom.Y = (calcPos.Y < boundingBoxMin.Y) ? boundingBoxMin.Y : (calcPos.Y > boundingBoxMax.Y) ? boundingBoxMax.Y : calcPos.Y;
//        posToCheckFrom.Z = (calcPos.Z < boundingBoxMin.Z) ? boundingBoxMin.Z : (calcPos.Z > boundingBoxMax.Z) ? boundingBoxMax.Z : calcPos.Z;
//        return Vector3.distanceSquared(calcPos, posToCheckFrom);
    }
	
	

}
