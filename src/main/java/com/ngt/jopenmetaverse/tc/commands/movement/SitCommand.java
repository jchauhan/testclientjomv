/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.movement;

import java.util.Map.Entry;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.types.Action;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.Vector3;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


public class SitCommand extends Command
    {
        public SitCommand(TestClient testClient)
		{
        	super(testClient);
			Name = "sit";
			Description = "Attempt to sit on the closest prim";
            Category = CommandCategory.Movement;
		}
			
        @Override
		public String Execute(String[] args, UUID fromAgentID)
		{
            final Primitive[] closest = new Primitive[]{null};
		    final double[] closestDistance = new double[]{Double.MAX_VALUE};

            Client.network.getCurrentSim().ObjectsPrimitives.foreach(
                new Action<Entry<Long, Primitive>>()
                {
					public void execute(Entry<Long, Primitive> t) {
						Primitive prim = t.getValue();
	                    float distance = Vector3.distance(Client.self.getSimPosition(), prim.Position);
						
						                    if (closest[0] == null || distance < closestDistance[0])
						                    {
						                        closest[0] = prim;
						                        closestDistance[0] = distance;
						                    }						
					}
                }
            );

            if (closest[0] != null)
            {
                Client.self.RequestSit(closest[0].ID, Vector3.Zero);
                Client.self.Sit();

                return "Sat on " + closest[0].ID + " (" + closest[0].LocalID + "). Distance: " + closestDistance;
            }
            else
            {
                return "Couldn't find a nearby prim to sit on";
            }
		}
    }