/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.communication;

import java.util.Observable;

import com.ngt.jopenmetaverse.shared.sim.AgentManager.ChatType;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.am.ChatEventArgs;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class EchoMasterCommand extends Command
{
    public EchoMasterCommand(TestClient testClient)
	{
    	super(testClient);
		Name = "echoMaster";
		Description = "Repeat everything that master says.";
        Category = CommandCategory.Communication;
	}

    @Override
    public String Execute(String[] args, UUID fromAgentID)
	{
		if (!Active)
		{
			Active = true;
//            Client.self.ChatFromSimulator += Self_ChatFromSimulator;
            Client.self.registerChatFromSimulator(new EventObserver<ChatEventArgs>()
            		{
						@Override
						public void handleEvent(Observable o, ChatEventArgs arg) {
							Self_ChatFromSimulator(o, arg);
						}            	
            		});
            
			return "Echoing is now on.";
		}
		else
		{
			Active = false;
//            Client.self.ChatFromSimulator -= Self_ChatFromSimulator;
			 Client.self.registerChatFromSimulator(new EventObserver<ChatEventArgs>()
	            		{
							@Override
							public void handleEvent(Observable o, ChatEventArgs arg) {
								// TODO Auto-generated method stub
								Self_ChatFromSimulator(o, arg);
							}            	
	            		});
			return "Echoing is now off.";
		}
	}

    void Self_ChatFromSimulator(Object sender, ChatEventArgs e)
    {
        if (e.getMessage().length() > 0 && (Client.MasterKey == e.getSourceID() || (Client.MasterName.equals(e.getFromName()) && !Client.AllowObjectMaster)))
            Client.self.Chat(e.getMessage(), 0, ChatType.Normal);
    }		
}