/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.inventory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.ClientManager;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class ScriptCommand extends Command
{
    public ScriptCommand(TestClient testClient)
    {
    	super(testClient);
        Name = "script";
        Description = "Reads TestClient commands from a file. One command per line, arguments separated by spaces. Usage: script [filename]";
        Category = CommandCategory.TestClient;
    }

    @Override
    public String Execute(String[] args, UUID fromAgentID)
    {
        if (args.length != 1)
            return "Usage: script [filename]";

        // Load the file
        List<String> lines = new ArrayList<String>();        
        try
        {
        BufferedReader br = new BufferedReader(
        		new FileReader(new File(args[0])));
        String line2 = null;
        while((line2 = br.readLine())!=null)
        	lines.add(line2);
        
        // Execute all of the commands
        for (String line: lines)
        {
            line = line.trim();
            if (line.length() > 0)
                ClientManager.getInstance().DoCommandAll(line, UUID.Zero);
        }
        }
        catch (Exception e) { return e.getMessage(); }
        return "Finished executing " + lines.size() + " commands";
    }
}