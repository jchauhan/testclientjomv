/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.rendering;

import java.util.ArrayList;
import java.util.List;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.protocol.primitives.TextureEntryFace;
import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.events.MethodDelegate;
import com.ngt.jopenmetaverse.shared.sim.events.asm.TextureDownloadCallbackArgs;
import com.ngt.jopenmetaverse.shared.sim.rendering.FaceData;
import com.ngt.jopenmetaverse.shared.sim.rendering.RenderPrimitive;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureLoadItem;
import com.ngt.jopenmetaverse.shared.sim.rendering.TextureManagerR;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.BoundingVolume;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.DetailLevel;
import com.ngt.jopenmetaverse.shared.sim.rendering.mesh.primitive.PrimitiveMesher;
import com.ngt.jopenmetaverse.shared.sim.rendering.platform.jclient.DefaultRenderingPrimitiveWithCameraWindow;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.types.EnumsPrimitive.ProfileCurve;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class DrawAllPrimitivesCommand extends Command
{
	TextureManagerR textureManager  = null;
	public DrawAllPrimitivesCommand(TestClient testClient)
	{
		super(testClient);
		Name = "drawallprimitives";
		Description = "Primitives Rendering Example";
		Category = CommandCategory.Rendering;    
		textureManager = new TextureManagerR(Client);
	}


	@Override
	public  String Execute(String[] args, UUID fromAgentID) throws Exception
	{
		final StringBuilder result = new StringBuilder();
		
		Simulator sim = Client.network.getCurrentSim();
		
		List<RenderPrimitive> renderPrimitives = new ArrayList<RenderPrimitive>();
		
		PrimitiveMesher primMesher = new PrimitiveMesher(Client); 
//		List<Primitive> mainPrims = sim.ObjectsPrimitives.FindAll(new Predicate<Primitive>()
//		{
//			public boolean match(Primitive p) 
//			{
//				return p.ParentID == 0;
//			}	
//		});

		List<Primitive> mainPrims = sim.ObjectsPrimitives.getChildren(0L);
		
		for (Primitive mainPrim : mainPrims)
		{
			if(mainPrim.PrimData == null)
				System.out.println("PrimData is null");
			System.out.println(mainPrim.PrimData.profileCurve + "Prim Data: " + ProfileCurve.get(mainPrim.PrimData.profileCurve));
			if(ProfileCurve.get(mainPrim.PrimData.profileCurve) != null)
				System.out.println("\t\t" + mainPrim.LocalID + " " + mainPrim.getType() + " " + mainPrim.PrimData.PCode);
			else
			{
				//TODO why are getting prim with type unknown 17
				System.out.println("Could Not get Prim type for : " +  mainPrim.ID.toString());
				continue;
			}
			
			RenderPrimitive rmainprim = UpdatePrimBlocking(primMesher, mainPrim);
			
			if(rmainprim.Faces == null)
			{
				//Why are we getting this
				System.out.println("Could Not generate Mesh for Prim: " +  rmainprim.getBasePrim().ID.toString());
				continue;
			}
			
			renderPrimitives.add(rmainprim);
			
			final long rootPrimId = mainPrim.LocalID;
			
			System.out.println("Main Prim: " + mainPrim.LocalID + " " + mainPrim.getType() + " " + mainPrim.PrimData.PCode);
			printTextures(rmainprim, mainPrim, "\t");
			
//			List<Primitive> childPrims = sim.ObjectsPrimitives.FindAll(new Predicate<Primitive>()
//					{
//				public boolean match(Primitive t) {
//					return t.ParentID == rootPrimId;
//				}
//					});

			List<Primitive> childPrims = sim.ObjectsPrimitives.getChildren(rootPrimId);
			
			for(Primitive subPrim: childPrims)
			{   
				if(subPrim.PrimData == null)
					System.out.println("PrimData is null");
				System.out.println(subPrim.PrimData.profileCurve + "Prim Data: " + ProfileCurve.get(subPrim.PrimData.profileCurve));
				if(ProfileCurve.get(subPrim.PrimData.profileCurve) != null)
					System.out.println("\t\t" + subPrim.LocalID + " " + subPrim.getType() + " " + subPrim.PrimData.PCode);
				else
				{
					//TODO why are getting prim with type unknown 17
					System.out.println("Could Not get Prim type for : " +  subPrim.ID.toString());
					continue;
				}
				
				
				System.out.println("Child Prim: " + subPrim.LocalID + " " + subPrim.getType() + " " + subPrim.PrimData.PCode);

				
				RenderPrimitive rsubprim = UpdatePrimBlocking(primMesher, subPrim);
				rsubprim.setParentSceneObject(rmainprim);
				if(rsubprim.Faces == null)
				{
					//Why are we getting this
					System.out.println("Could Not generate Mesh for Prim: " +  rsubprim.getBasePrim().ID.toString());
					continue;
				}
				renderPrimitives.add(rsubprim);
				printTextures(rsubprim, subPrim, "\t\t\t");
			}
		}

//		DefaultRenderingPrimitiveWindow demo = new DefaultRenderingPrimitiveWindow(Client.network.getCurrentSim().Stats,  textureManager, renderPrimitives);
		
		DefaultRenderingPrimitiveWithCameraWindow demo = new DefaultRenderingPrimitiveWithCameraWindow(Client,  textureManager, renderPrimitives);

		
        // set title, window size
        demo.window_title = "Current Terrain";
        demo.displayWidth = 640;
        demo.displayHeight = 480;
		
        demo.run();
		return result.toString();
	}

	public RenderPrimitive UpdatePrimBlocking(PrimitiveMesher primMesher, Primitive prim) throws Exception
	{

		RenderPrimitive rPrim = new RenderPrimitive();
		rPrim.setBasePrim(prim);
		rPrim.Meshed = false;
		rPrim.BoundingVolume = new BoundingVolume();
		rPrim.BoundingVolume.FromScale(prim.Scale);         
		primMesher.MeshPrim(rPrim, true, DetailLevel.High, DetailLevel.High, DetailLevel.Highest);
		return rPrim;
	}

	void printTextures(RenderPrimitive rprim, Primitive prim, String indent) throws Exception
	{
		for(int i =0;  i < rprim.Faces.size(); i ++)
		{
			TextureEntryFace tef = prim.Textures.GetFace(i);
			System.out.println(String.format(indent + "Face Index: %d, Texture ID: %s", i, tef.getTextureID()));
			if(!Client.assets.Cache.hasAsset(tef.getTextureID()))
			{
				System.out.println(indent + "Downloading textures ... ");
				Client.assets.RequestImage(tef.getTextureID(), getTextureDownloadCallback(tef.getTextureID().toString()));
			}
			textureManager.requestDownloadTexture(new TextureLoadItem((FaceData)rprim.Faces.get(i).UserData, 
					prim, prim.Textures.GetFace(i)));
		}
	}
	
	public MethodDelegate<Void, TextureDownloadCallbackArgs>  getTextureDownloadCallback(final String filename)
	{
		return new MethodDelegate<Void, TextureDownloadCallbackArgs>()
		{
			public Void execute(TextureDownloadCallbackArgs e) 
			{
				switch (e.getState())
				{
				case Finished:
					Client.assets.Cache.saveAssetToCache(filename, e.getAssetTexture().AssetData);
					break;

				case Aborted:
				case NotFound:
				case Timeout:
					break;
				}
				return null;
			}

		};
	}

}
