/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.inventory;


import java.util.EnumSet;
import java.util.List;

import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;
import com.ngt.jopenmetaverse.shared.exception.nm.InventoryException;
import com.ngt.jopenmetaverse.shared.protocol.primitives.Permissions.PermissionMask;
import com.ngt.jopenmetaverse.shared.sim.InventoryManager;
import com.ngt.jopenmetaverse.shared.sim.InventoryManager.InventoryFolder;
import com.ngt.jopenmetaverse.shared.sim.inventory.Inventory;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryBase;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryItem;
import com.ngt.jopenmetaverse.shared.types.UUID;

 public class ListContentsCommand extends Command
    {
        private InventoryManager Manager;
        private Inventory Inventory;
        public ListContentsCommand(TestClient client)
        {
        	super(client);
            Name = "ls";
            Description = "Lists the contents of the current working inventory folder.";
            Category = CommandCategory.Inventory;
        }
        @Override
        public String Execute(String[] args, UUID fromAgentID) throws InventoryException
        {
            if (args.length > 1)
                return "Usage: ls [-l]";
            boolean longDisplay = false;
            if (args.length > 0 && args[0].equals("-l"))
                longDisplay = true;

            Manager = Client.inventory;
            Inventory = Manager.getStore();
            // WARNING: Uses local copy of inventory contents, need to download them first.
            List<InventoryBase> contents = Inventory.GetContents(Client.CurrentDirectory);
            String displayString = "";
            String nl = "\n"; // New line character
            System.out.println("Fetched Contents: " + contents.size());
            // Pretty simple, just print out the contents.
            for (InventoryBase b : contents)
            {
                if (longDisplay)
                {
                    // Generate a nicely formatted description of the item.
                    // It kinda looks like the output of the unix ls.
                    // starts with 'd' if the inventory is a folder, '-' if not.
                    // 9 character permissions String
                    // UUID of object
                    // Name of object
                    if (b instanceof InventoryFolder)
                    {
                        InventoryFolder folder = (InventoryFolder)b ;
                        displayString += "d--------- ";
                        displayString += folder.UUID;
                        displayString += " " + folder.Name;
                    }
                    else if (b instanceof InventoryItem)
                    {
                        InventoryItem item = (InventoryItem)b ;
                        displayString += "-";
                        displayString += PermMaskString(item.Permissions.OwnerMask);
                        displayString += PermMaskString(item.Permissions.GroupMask);
                        displayString += PermMaskString(item.Permissions.EveryoneMask);
                        displayString += " " + item.UUID;
                        displayString += " " + item.Name;
                        displayString += nl;
                        displayString += "  AssetID: " + item.AssetUUID;
                    }
                }
                else
                {
                    displayString += b.Name;
                }
                displayString += nl;
            }
            return displayString;
        }

        /// <summary>
        /// Returns a 3-character summary of the PermissionMask
        /// CMT if the mask allows copy, mod and transfer
        /// -MT if it disallows copy
        /// --T if it only allows transfer
        /// --- if it disallows everything
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        private static String PermMaskString(EnumSet<PermissionMask> ownerMask)
        {
            String str = "";
            if (((long)PermissionMask.getIndex(ownerMask) | (long)PermissionMask.Copy.getIndex()) == (long)PermissionMask.Copy.getIndex())
                str += "C";
            else
                str += "-";
            if (((long)PermissionMask.getIndex(ownerMask) | (long)PermissionMask.Modify.getIndex()) == (long)PermissionMask.Modify.getIndex())
                str += "M";
            else
                str += "-";
            if (((long)PermissionMask.getIndex(ownerMask) | (long)PermissionMask.Transfer.getIndex()) == (long)PermissionMask.Transfer.getIndex())
                str += "T";
            else
                str += "-";
            return str;
        }
    }