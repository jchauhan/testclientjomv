/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.prims;

import java.util.Observable;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.sim.events.AutoResetEvent;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.om.ObjectPropertiesEventArgs;
import com.ngt.jopenmetaverse.shared.types.Predicate;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

 public class PrimInfoCommand extends Command
    {
        public PrimInfoCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "priminfo";
            Description = "Dumps information about a specified prim. " + "Usage: priminfo [prim-uuid]";
            Category = CommandCategory.Objects;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID) throws InterruptedException
        {
        	final StringBuilder result = new StringBuilder();
            final UUID[] primID = new UUID[1];

            if (args.length != 1)
                return "Usage: priminfo [prim-uuid]";

            if (Utils.tryParseUUID(args[0], primID))
            {
                Primitive target = Client.network.getCurrentSim().ObjectsPrimitives.Find(
                    new Predicate<Primitive>()
                    {
						public boolean match(Primitive prim) {
	                    	return prim.ID.equals(primID[0]); 
						} 
                    }
                );

                if (target != null)
                {
                    if (target.Text.equals(""))
                    {
                        result.append("\nText: " + target.Text);
                    }
                    if(target.Light != null)
                        result.append("\nLight: " + target.Light.ToString());

                    if (target.ParticleSys.CRC != 0)
                        result.append("\nParticles: " + target.ParticleSys.toString());

                    result.append("TextureEntry:");
                    if (target.Textures != null)
                    {
                        result.append(String.format("\nDefault texure: %s",
                            target.Textures.DefaultTexture.getTextureID().toString()));

                        for (int i = 0; i < target.Textures.FaceTextures.length; i++)
                        {
                            if (target.Textures.FaceTextures[i] != null)
                            {
                                result.append(String.format("\nFace %d: %s", i,
                                    target.Textures.FaceTextures[i].getTextureID().toString()));
                            }
                        }
                    }
                    else
                    {
                        result.append("\nnull");
                    }

                    final AutoResetEvent propsEvent = new AutoResetEvent(false);
                    EventObserver<ObjectPropertiesEventArgs> propsCallback = new EventObserver<ObjectPropertiesEventArgs>()
                    		{
								@Override
								public void handleEvent(Observable o,
										ObjectPropertiesEventArgs e) 
								{
		                            result.append(String.format(
	                                "Category: %s\nFolderID: %s\nFromTaskID: %s\nInventorySerial: %s\nItemID: %s\nCreationDate: %s",
	                                e.getProperties().Category, e.getProperties().FolderID.toString(), 
	                                e.getProperties().FromTaskID.toString(), e.getProperties().InventorySerial, 
	                                e.getProperties().ItemID.toString(), e.getProperties().CreationDate));
	                            propsEvent.set();									
								}
                    	
//                        public void handleEvent(Object sender, ObjectPropertiesEventArgs e)
//                        {
//                            JLogger.info(String.Format(
//                                "Category: {0}\nFolderID: {1}\nFromTaskID: {2}\nInventorySerial: {3}\nItemID: {4}\nCreationDate: {5}",
//                                e.getProperties().Category, e.getProperties().FolderID, e.getProperties().FromTaskID, e.getProperties().InventorySerial, 
//                                e.getProperties().ItemID, e.getProperties().CreationDate), Helpers.LogLevel.Info);
//                            propsEvent.Set();
//                        }
                        };

                    Client.objects.registerOnObjectProperties(propsCallback);
                    Client.objects.SelectObject(Client.network.getCurrentSim(), target.LocalID, true);
                    propsEvent.waitOne(1000 * 10);
                    Client.objects.unregisterOnObjectProperties(propsCallback);
                    return result.toString();
                }
                else
                {
                    return "Could not find prim " + primID.toString();
                }
            }
            else
            {
                return "Usage: priminfo [prim-uuid]";
            }
        }
    }