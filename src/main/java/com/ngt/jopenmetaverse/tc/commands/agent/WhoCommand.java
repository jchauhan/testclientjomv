/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.agent;

import java.util.Map.Entry;
import com.ngt.jopenmetaverse.shared.sim.Avatar;
import com.ngt.jopenmetaverse.shared.types.Action;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;

public class WhoCommand extends Command
    {
        public WhoCommand(TestClient testClient)
		{
        	super(testClient);
			Name = "who";
			Description = "Lists seen avatars.";
            Category = CommandCategory.Other;
		}

        @Override
        public String Execute(String[] args, UUID fromAgentID)
		{
			final StringBuilder result = new StringBuilder();

            synchronized (Client.network.Simulators)
            {
                for (int i = 0; i < Client.network.Simulators.size(); i++)
                {
                    Client.network.Simulators.get(i).ObjectsAvatars.foreach(new Action<Entry<Long, Avatar>>()
                    {
						public void execute(Entry<Long, Avatar> t) {
	                    	Avatar av = t.getValue();
                          result.append(String.format("\n%1$s (Group: %2$s, Location: %3$s, UUID: %4$s LocalID: %5$s)",
                              av.getName(), av.getGroupName(), av.Position, av.ID, av.LocalID));
					}
                  });
                }
            }
            return result.toString();
		}
    }