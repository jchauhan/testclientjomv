/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.debug.rendering;

import java.io.File;

import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.rendering.terrain.TerrainSplat;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.FileUtils;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;


public class DumpTerrainTextures extends Command
    {
		//uint
        long SerialNum = 2;

        public DumpTerrainTextures(TestClient testClient)
        {
        	super(testClient);
            Name = "rendering.dumpterraintextures";
            Description = "Dump Textures Every t seconds Usage: rendering.dumptextures [t secs] [outputPath]";
            Category = CommandCategory.Debug;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID) throws Exception
        {
           StringBuilder sb = new StringBuilder();
           
           if(args.length < 2)
               return "Usage: rendering.dumptextures [t secs] [outputPath]";

           int secs = Integer.parseInt(args[0]);
           String outputPath = args[1];

           int totalTime = 0;
           long curTimeStamp = Utils.getUnixTime();
           
           Simulator sim = Client.network.getCurrentSim();
           UUID[] textureIds = new UUID[] { sim.TerrainDetail0, sim.TerrainDetail1, sim.TerrainDetail2, sim.TerrainDetail3 };
           IBitmap[] bitmaps = TerrainSplat.downloadAndDecodeTextures(Client, textureIds);           

           for(int i = 0; i < bitmaps.length; i++)
           {
        	   if(bitmaps[i] == null)
        		   System.out.println("Could not download texture " + textureIds[i]);
        	   else
        	   {   
        		   String finalFileName = FileUtils.combineFilePath(outputPath,  curTimeStamp + "-" + totalTime + "-" + i);
//        		   bitmaps[i].dumpToFile(finalFileName);

        		   FileUtils.writeBytes(new File(finalFileName + ".tga"), bitmaps[i].exportTGA());
        	   }
           }
           return sb.toString();
        }
    }