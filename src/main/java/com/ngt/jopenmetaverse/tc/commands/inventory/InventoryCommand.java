/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc.commands.inventory;

import java.util.List;

import com.ngt.jopenmetaverse.shared.exception.nm.InventoryException;
import com.ngt.jopenmetaverse.shared.sim.InventoryManager;
import com.ngt.jopenmetaverse.shared.sim.InventoryManager.InventoryFolder;
import com.ngt.jopenmetaverse.shared.sim.InventoryManager.InventorySortOrder;
import com.ngt.jopenmetaverse.tc.TestClient;
import com.ngt.jopenmetaverse.tc.commands.Command;
import com.ngt.jopenmetaverse.tc.commands.CommandCategory;
import com.ngt.jopenmetaverse.shared.sim.inventory.Inventory;
import com.ngt.jopenmetaverse.shared.sim.inventory.InventoryBase;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

 public class InventoryCommand extends Command
    {
        private Inventory Inventory;
        private InventoryManager Manager;

        public InventoryCommand(TestClient testClient)
        {
        	super(testClient);
            Name = "i";
            Description = "Prints out inventory.";
            Category = CommandCategory.Inventory;
        }

        @Override
        public String Execute(String[] args, UUID fromAgentID)
        {
        	try{
            Manager = Client.inventory;
            Inventory = Manager.getStore();

            StringBuilder result = new StringBuilder();

        	System.out.println("Getting Root Folder");
            InventoryFolder rootFolder = Inventory.getRootFolder();
        	System.out.println("Printing Folder");
            PrintFolder(rootFolder, result, 0);

            return result.toString();
        	}
        	catch(Exception e)
        	{JLogger.warn("Error while executing command" + Description 
        			+ "\n" +Utils.getExceptionStackTraceAsString(e));
        	System.out.println("Error: " + e.getMessage());
        	return "Error: " + e.getMessage();
        	}
        }

        void PrintFolder(InventoryFolder f, StringBuilder result, int indent) throws InterruptedException, InventoryException
        {
            List<InventoryBase> contents = Manager.FolderContents(f.UUID, Client.self.getAgentID(),
                true, true, InventorySortOrder.ByName, 3000);

            if (contents != null)
            {
                for (InventoryBase i : contents)
                {
//                    result.append(String.format("%" + indent*2 + "s (%s)\n",i.Name, i.UUID));
                	String fmt = indent > 0 ? "%" + indent*2 + "s%s (%s)\n" : "%s%s (%s)\n";
                    result.append(String.format(fmt, "", i.Name, i.UUID));
                    if (i instanceof InventoryFolder)
                    {
                        InventoryFolder folder = (InventoryFolder)i;
                        PrintFolder(folder, result, indent + 1);
                    }
                }
            }
        }
    }