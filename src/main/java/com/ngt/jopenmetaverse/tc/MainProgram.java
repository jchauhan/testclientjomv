/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.ngt.jopenmetaverse.shared.sim.ApplicationContextJclientImpl;
import com.ngt.jopenmetaverse.shared.sim.NetworkManager;
import com.ngt.jopenmetaverse.shared.sim.Settings;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class MainProgram {
    public static String LoginURI;

    private static void Usage()
    {
        System.out.println("Usage: \n"  +
                "TestClient.exe [--first firstname --last lastname --pass password] [--file userlist.txt] [--loginuri=\"uri\"] [--startpos \"sim/x/y/z\"] [--master \"master name\"] [--masterkey \"master uuid\"] [--gettextures] [--scriptfile \"filename\"]");
    }

    public static void main(String[] args)
    {
    	//ONLY To test android specific jpeg 2000 library
//    	ApplicationContextJclientImpl context = new ApplicationContextJclientImpl();
    	ApplicationContextAndroidStubImpl context = new ApplicationContextAndroidStubImpl();
    	context.init();
    	
    	try
    	{
    	CommandLine arguments = parse(args);
    	//Do not print eveything
//    	JLogger.setLevel(Level.SEVERE);
//    	for(Handler h: JLogger.getLogger("Global").getHandlers())
//    	{
//    		JLogger.getLogger("Global").removeHandler(h);
//    	}
    	LogManager.getLogManager().reset();
    	Logger globalLogger = Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);
    	globalLogger.setLevel(java.util.logging.Level.OFF);
    	
    	JLogger.addHandler(new FileHandler("Logger"));
        List<LoginDetails> accounts = new ArrayList<LoginDetails>();
        LoginDetails account;
        boolean groupCommands = false;
        String masterName = "";
        UUID masterKey = UUID.Zero;
        String file = "";
        boolean getTextures = false;
        boolean noGUI = false; // true if to not prompt for input
        String scriptFile = "";
        
        if (arguments.hasOption("groupcommands"))
            groupCommands = true;

        if (arguments.hasOption("masterkey"))
            masterKey = UUID.Parse((String)arguments.getOptionValue("masterkey"));

        if (arguments.hasOption("master"))
            masterName = (String) arguments.getOptionValue("master");

        if (arguments.hasOption("loginuri"))
            LoginURI = (String) arguments.getOptionValue("loginuri");
        
        System.out.println((String) arguments.getOptionValue("loginuri"));
        
        if (Utils.isNullOrEmpty(LoginURI))
            LoginURI = Settings.AGNI_LOGIN_SERVER;
        
        JLogger.info("Using login URI " + LoginURI);

        if (arguments.hasOption("gettextures"))
            getTextures = true;

        if (arguments.hasOption("nogui"))
            noGUI = true;

        if (arguments.hasOption("scriptfile"))
        {
            scriptFile = (String) arguments.getOptionValue("scriptfile");
            if (! (new File(scriptFile)).exists() )
            {
                JLogger.error(String.format("File {0} Does not exist", scriptFile));
                return;
            }
        }

        if (arguments.hasOption("file"))
        {
            file = (String) arguments.getOptionValue("file");

            if (!(new File(file)).exists())
            {
                JLogger.error(String.format("File {0} Does not exist", file));
                return;
            }

            // Loading names from a file
            try
            {
            	BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
                    String line;
                    int lineNumber = 0;

                    while ((line = reader.readLine()) != null)
                    {
                        lineNumber++;
                        String[] tokens = line.trim().split("\\s|,");

                        if (tokens.length >= 3)
                        {
                            account = new LoginDetails();
                            account.FirstName = tokens[0];
                            account.LastName = tokens[1];
                            account.Password = tokens[2];

                            if (tokens.length >= 4) // Optional starting position
                            {
                                String sep = "/";
                                String[] startbits = tokens[3].split(sep);
                                account.StartLocation = NetworkManager.StartLocation(startbits[0], Integer.parseInt(startbits[1]),
                                    Integer.parseInt(startbits[2]), Integer.parseInt(startbits[3]));
                            }

                            accounts.add(account);
                        }
                        else
                        {
                            JLogger.warn("Invalid data on line " + lineNumber +
                                ", must be in the format of: FirstName LastName Password [Sim/StartX/StartY/StartZ]");
                        }
                    }
            }
            catch (Exception ex)
            {
                JLogger.error("Error reading from " + args[1]);
                return;
            }
        }
        else if (arguments.getOptionValue("first") != null 
        		&& arguments.getOptionValue("last") != null 
        		&& arguments.getOptionValue("pass") != null)
        {
            // Taking a single login off the command-line
            account = new LoginDetails();
            account.FirstName = (String) arguments.getOptionValue("first");
            account.LastName = (String) arguments.getOptionValue("last");
            account.Password = (String) arguments.getOptionValue("pass");

            accounts.add(account);
        }
        else if (arguments.getOptionValue("help") != null)
        {
            Usage();
            return;
        }

        for (LoginDetails a : accounts)
        {
            a.GroupCommands = groupCommands;
            a.MasterName = masterName;
            a.MasterKey = masterKey;
            a.URI = LoginURI;

            if (arguments.getOptionValue("startpos") != null)
            {
                String sep = "/";
                String[] startbits = ((String)arguments.getOptionValue("startpos")).split(sep);
                a.StartLocation = NetworkManager.StartLocation(startbits[0], Integer.parseInt(startbits[1]),
                        Integer.parseInt(startbits[2]), Integer.parseInt(startbits[3]));
            }
        }

        // Login the accounts and run the input loop
        ClientManager.Instance.Start(accounts, getTextures);

        if (!Utils.isNullOrEmpty(scriptFile))
            ClientManager.Instance.DoCommandAll("script " + scriptFile, UUID.Zero);

        // Then Run the ClientManager normally
        ClientManager.Instance.Run(noGUI);
    	}
    	catch(Exception e)
    	{
    		JLogger.error("Exception: " + Utils.getExceptionStackTraceAsString(e));
    	}
    }
   
    private static CommandLine parse(String[] args) throws ParseException
    {
    	
    	CommandLineParser parser = new PosixParser();
    	Options options = createOptions();
    	CommandLine cmd = parser.parse( options, args);
//    	for(Object a: cmd.getArgList())
//    	{
//    		System.out.println((String)a);
//    	}
    	HelpFormatter formatter = new HelpFormatter();
    	formatter.printHelp( "testclient", options );
    	return cmd;
    }
    
	@SuppressWarnings("static-access")
    private static Options createOptions()
    {
    	// create Options object
    	Options options = new Options();

//    String a = "TestClient.exe [--first firstname --last lastname --pass password] [--file userlist.txt] " +
//    "[--loginuri=\"uri\"] [--startpos \"sim/x/y/z\"] [--master \"master name\"] [--masterkey \"master uuid\"] [--gettextures] [--scriptfile \"filename\"]");

    	
		Option firstname   = OptionBuilder.withArgName( "firstname" )
                .hasArg()
                .withDescription(  "First Name" )
                .create( "first" );

    	Option lastname   = OptionBuilder.withArgName( "lastname" )
                .hasArg()
                .withDescription(  "Last Name" )
                .create( "last" );
    	
    	Option password   = OptionBuilder.withArgName( "password" )
                .hasArg()
                .withDescription(  "First Name" )
                .create( "pass" );
    	
    	Option file  = OptionBuilder.withArgName( "file" )
                .hasArg()
                .withDescription(  "User File List" )
                .create( "file" );
    	
    	Option loginuri   = OptionBuilder.withArgName( "loginurl" )
                .hasArg()
                .withDescription(  "\"uri\"" )
                .create( "loginuri" );
    	

    	Option startpos   = OptionBuilder.withArgName( "startpos" )
                .hasArg()
                .withDescription(  "\"sim/x/y/z\"" )
                .create( "startpos" );
    	
    	Option master   = OptionBuilder.withArgName( "master" )
                .hasArg()
                .withDescription(  "\"master name\"" )
                .create( "master" );
    	
    	Option masterkey   = OptionBuilder.withArgName( "master" )
                .hasArg()
                .withDescription(  "\"master uuid\"" )
                .create( "masterkey" );
    	
    	Option scriptfile   = OptionBuilder.withArgName( "scriptfile" )
                .hasArg()
                .withDescription(  "\"filename\"" )
                .create( "scriptfile" );
    	
    	
    	// add t option
    	options.addOption(firstname);
    	options.addOption(lastname);
    	options.addOption(password);
    	options.addOption(file);
    	options.addOption(loginuri);
    	options.addOption(startpos);
    	options.addOption(master);
    	options.addOption(masterkey);
    	options.addOption(scriptfile);
    	options.addOption("gettextures", false, "Whether to get textures");
    	options.addOption("help", false, "Print this Help Message");

    	
    	return options;
    }
    
    
}
