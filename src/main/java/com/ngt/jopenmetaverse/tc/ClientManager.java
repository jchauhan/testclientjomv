/**
 * A library to interact with Virtual Worlds such as OpenSim
 * Copyright (C) 2012  Jitendra Chauhan, Email: jitendra.chauhan@gmail.com
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.ngt.jopenmetaverse.tc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicInteger;

import com.ngt.jopenmetaverse.shared.protocol.primitives.Primitive;
import com.ngt.jopenmetaverse.shared.sim.GridClient;
import com.ngt.jopenmetaverse.shared.sim.NetworkManager;
import com.ngt.jopenmetaverse.shared.sim.Simulator;
import com.ngt.jopenmetaverse.shared.sim.events.EventObserver;
import com.ngt.jopenmetaverse.shared.sim.events.ThreadPoolFactory;
import com.ngt.jopenmetaverse.shared.sim.events.dm.DirPeopleReplyEventArgs;
import com.ngt.jopenmetaverse.shared.sim.login.LoginParams;
import com.ngt.jopenmetaverse.shared.sim.login.LoginProgressEventArgs;
import com.ngt.jopenmetaverse.shared.sim.login.LoginStatus;
import com.ngt.jopenmetaverse.shared.types.UUID;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.PlatformUtils;
import com.ngt.jopenmetaverse.shared.util.Utils;
import com.ngt.jopenmetaverse.tc.commands.inventory.ScriptCommand;
import com.ngt.jopenmetaverse.tc.commands.system.WaitForLoginCommand;

public class ClientManager {
	final String VERSION = "1.0.0";

	static final ClientManager Instance = new ClientManager(); 
	public static ClientManager getInstance() { return Instance; } 

	public Map<UUID, TestClient> Clients = new HashMap<UUID, TestClient>();
	public Map<Simulator, Map<Long, Primitive>> SimPrims = new HashMap<Simulator, Map<Long, Primitive>>();

	public boolean Running = true;
	public boolean GetTextures = false;
	public volatile int PendingLogins = 0;
	public String onlyAvatar = "";

	ClientManager()
	{
	}

	public void Start(List<LoginDetails> accounts, boolean getTextures) throws Exception
	{
		GetTextures = getTextures;

		for (LoginDetails account : accounts)
			Login(account);
	}

	public TestClient Login(String[] args) throws Exception
	{
		if (args.length < 3)
		{
			System.out.println("Usage: login firstname lastname password [simname] [login server url]");
			return null;
		}
		LoginDetails account = new LoginDetails();
		account.FirstName = args[0];
		account.LastName = args[1];
		account.Password = args[2];

		if (args.length > 3)
		{
			// If it looks like a full starting position was specified, parse it
			if (args[3].startsWith("http"))
			{
				account.URI = args[3];
			}
			else
			{
				if (args[3].indexOf('/') >= 0)
				{
					String sep = "/";
					String[] startbits = args[3].split(sep);
					account.StartLocation = NetworkManager.StartLocation(startbits[0], Integer.parseInt(startbits[1]),
							Integer.parseInt(startbits[2]), Integer.parseInt(startbits[3]));
				}

				// Otherwise, use the center of the named region
				if (account.StartLocation == null)
					account.StartLocation = NetworkManager.StartLocation(args[3], 128, 128, 40);
			}
		}

		if (args.length > 4)
			if (args[4].startsWith("http"))
				account.URI = args[4];

		if (Utils.isNullOrEmpty(account.URI))
			account.URI = MainProgram.LoginURI;
		JLogger.info("Using login URI " + account.URI);

		return Login(account);
	}

	public TestClient Login(final LoginDetails account) throws Exception
	{
		// Check if this client is already logged in
		for (TestClient c : Clients.values())
		{
			if (c.self.getFirstName().equals(account.FirstName) && c.self.getFirstName().equals(account.LastName))
			{
				Logout(c);
				break;
			}
		}

		++PendingLogins;

		final TestClient client = new TestClient(this);

		client.network.RegisterLoginProgressCallback(new EventObserver<LoginProgressEventArgs>()
				{
			@Override
			public void handleEvent(Observable sender,
					LoginProgressEventArgs e) {
				JLogger.info(String.format("Login %s: %s", e.getStatus(), e.getMessage()));

				if (e.getStatus().equals(LoginStatus.Success))
				{
					//TODO Implement
					Clients.put(client.self.getAgentID(), client);

					if (client.MasterKey.equals(UUID.Zero))
					{
						UUID query = UUID.Zero;
						final UUID tmpquery = query;
						EventObserver<DirPeopleReplyEventArgs> peopleDirCallback = new EventObserver<DirPeopleReplyEventArgs>()
								{
							@Override
							public void handleEvent(Observable sender2, DirPeopleReplyEventArgs dpe)
							{
								if (dpe.getQueryID().equals(tmpquery))
								{
									if (dpe.getMatchedPeople().size() != 1)
									{
										JLogger.warn("Unable to resolve master key from " + client.MasterName);
									}
									else
									{
										client.MasterKey = dpe.getMatchedPeople().get(0).AgentID;
										JLogger.info("Master key resolved to " + client.MasterKey);
									}
								}
							}
								};

								client.directory.registerOnDirPeopleReply(peopleDirCallback);
								query = client.directory.StartPeopleSearch(client.MasterName, 0);
					}

					JLogger.info("Logged in " + client.toString());
					--PendingLogins;
				}
				else if (e.getStatus().equals(LoginStatus.Failed))
				{
					JLogger.warn("Failed to login " + account.FirstName + " " + account.LastName + ": " +
							client.network.getLoginMessage());
					--PendingLogins;
				}
			}
				});

		// Optimize the throttle
		client.throttle.setWind(0);
		client.throttle.setCloud(0);
		client.throttle.setLand(1000000);
		client.throttle.setTask(1000000);

		client.GroupCommands = account.GroupCommands;
		client.MasterName = account.MasterName;
		client.MasterKey = account.MasterKey;
		client.AllowObjectMaster = client.MasterKey != UUID.Zero; // Require UUID for object master.

		LoginParams loginParams = client.network.DefaultLoginParams(
				account.FirstName, account.LastName, account.Password, "TestClient", VERSION);

		if (!Utils.isNullOrEmpty(account.StartLocation))
			loginParams.Start = account.StartLocation;

		if (!Utils.isNullOrEmpty(account.URI))
			loginParams.URI = account.URI;

		client.network.BeginLogin(loginParams);
		return client;
	}

	/// <summary>
	/// 
	/// </summary>
	public void Run(boolean noGUI) throws Exception
	{
		if (noGUI)
		{
			while (Running)
			{
				PlatformUtils.sleep(2 * 1000);
			}
		}
		else {
			System.out.println("Type quit to exit.  Type help for a command list.");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			while (Running)
			{
				PrintPrompt();
				try {
					String input = br.readLine();
					DoCommandAll(input, UUID.Zero);
				} catch (IOException ioe) {
					System.out.println("IO error trying to read your name!");
					System.exit(1);
				}
			}
		}

		for (GridClient client : Clients.values())
		{
			if (client.network.getConnected())
				client.network.Logout();
		}
	}

	private void PrintPrompt()
	{
		int online = 0;

		for (GridClient client : Clients.values())
		{
			if (client.network.getConnected()) online++;
		}

		System.out.print(online + " avatars online> ");
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="cmd"></param>
	/// <param name="fromAgentID"></param>
	/// <param name="imSessionID"></param>
	public void DoCommandAll(String cmd, UUID fromAgentID) throws Exception
	{
		System.out.println("Raw Command: " + cmd);
		String[] tokens = cmd.trim().split("[ \\\t]");
		if (tokens.length == 0)
			return;

		String firstToken = tokens[0].toLowerCase();
		if (Utils.isNullOrEmpty(firstToken))
			return;

		// Allow for comments when cmdline begins with ';' or '#'
		if (firstToken.charAt(0) == ';' || firstToken.charAt(0) == '#')
			return;

		if ('@' == firstToken.charAt(0)) {
			onlyAvatar = "";
			if (tokens.length == 3) {
				boolean found = false;
				onlyAvatar = tokens[1]+" "+tokens[2];
				for (TestClient client :Clients.values()) {
					if ((client.toString().equals(onlyAvatar)) && (client.network.getConnected())) {
						found = true;
						break;
					}
				}
				if (found) {
					JLogger.warn("Commanding only "+onlyAvatar+" now");
				} else {
					JLogger.info("Commanding nobody now. Avatar "+onlyAvatar+" is offline");
				}
			} else {
				JLogger.info("Commanding all avatars now");
			}
			return;
		}

		String[] args = null;
		if ((tokens.length -1) > 0)
		{
			args = Arrays.copyOfRange(tokens, 1, tokens.length);
		}
		else
		{
			args = new String[tokens.length - 1];
		}

		if (firstToken.equals("login"))
		{
			Login(args);
		}
		else if (firstToken.equals("quit"))
		{
			Quit();
			JLogger.info("All clients logged out and program finished running.");
		}
		else if (firstToken.equals("help"))
		{
			if (Clients.size() > 0)
			{
				for (TestClient client : Clients.values())
				{
					System.out.println(client.commands.get("help").Execute(args, UUID.Zero));
					break;
				}
			}
			else
			{
				System.out.println("You must login at least one bot to use the help command\n");
			}
		}
		else if (firstToken.equals("script"))
		{
			// No reason to pass this to all bots, and we also want to allow it when there are no bots
			ScriptCommand command = new ScriptCommand(null);
			JLogger.info(command.Execute(args, UUID.Zero));
		}
		else if (firstToken.equals("waitforlogin"))
		{
			// Special exception to allow this to run before any bots have logged in
			if (ClientManager.Instance.PendingLogins > 0)
			{
				WaitForLoginCommand command = new WaitForLoginCommand(null);
				JLogger.info(command.Execute(args, UUID.Zero));
			}
			else
			{
				JLogger.info("No pending logins");
			}
		}
		else
		{
			// Make an immutable copy of the Clients dictionary to safely iterate over
			Map<UUID, TestClient> clientsCopy = new HashMap<UUID, TestClient>(Clients);

			final AtomicInteger completed = new AtomicInteger(0);

			for(final TestClient client : clientsCopy.values())
			{
				final String tmpfirstToken = firstToken; 
				final UUID tmpfromAgentID = fromAgentID;
				final String[] tmpArgs = args;
				//            	System.out.println(firstToken + ":" + onlyAvatar + ":" + client.toString());
				ThreadPoolFactory.getThreadPool().execute(new Runnable(){
					public void run()
					{
						TestClient testClient = (TestClient)client;
						if (("".equals(onlyAvatar)) || (testClient.toString().equals(onlyAvatar))) {
							if (client.commands.contains(tmpfirstToken)) {
								System.out.println("Running the command... "  + tmpfirstToken);                        	  
								String result;
								try
								{
									result = client.commands.get(tmpfirstToken).Execute(tmpArgs, tmpfromAgentID);
									JLogger.info(result);
									System.out.println(result);
								}
								catch(Exception e)
								{   System.out.println((String.format("%s raised exception %s", tmpfirstToken, Utils.getExceptionStackTraceAsString(e))));
								JLogger.info(String.format("%s raised exception %s", tmpfirstToken, e));}
							}
							else
								JLogger.warn("Unknown command " + tmpfirstToken);
						}
						completed.incrementAndGet();
					}
				});
			}

			while (completed.get() < clientsCopy.size())
				PlatformUtils.sleep(50);
		}
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="client"></param>
	public void Logout(TestClient client)
	{
		Clients.remove(client.self.getAgentID());
		client.network.Logout();
	}

	/// <summary>
	/// 
	/// </summary>
	public void Quit()
	{
		Running = false;
		// TODO: It would be really nice if we could figure out a way to abort the ReadLine here in so that Run() will exit.
	}
}
