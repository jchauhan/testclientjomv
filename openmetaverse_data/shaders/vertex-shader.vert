attribute vec3 vertexPosition;
attribute vec2 texPosition;
attribute vec3 aVertexNormal;
varying vec2 texCoord;

uniform mat4 projectionMatrix;


void main() {
	gl_Position = projectionMatrix * vec4(vertexPosition, 1.0);
	texCoord = texPosition;
	vec4 transformedNormal  = projectionMatrix*vec4(aVertexNormal, 1);
}