#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D tex;
varying vec2 texCoord;
varying vec3 vLightWeighting;

void main() {	
	vec4 textureColor = texture2D(tex, texCoord);
    gl_FragColor = vec4(textureColor.rgb * vLightWeighting, textureColor.a);	
}