attribute vec3 vertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 texPosition;

uniform mat4 projectionMatrix;

uniform vec3 uAmbientColor;
uniform vec3 uLightingDirection;
uniform vec3 uDirectionalColor;
uniform bool uUseLighting;
uniform mat4 normalMatrix;

varying vec3 vLightWeighting;
varying vec2 texCoord;

void main() {
	gl_Position = (projectionMatrix) * vec4(vertexPosition, 1.0);
	texCoord = texPosition;

	if (!uUseLighting) {
      vLightWeighting = vec3(1.0, 1.0, 1.0);
    } else {
    	vec4 transformedNormal = normalMatrix * vec4(aVertexNormal, 1.0);
      float directionalLightWeighting = max(dot(transformedNormal.xyz, uLightingDirection), 0.0);
      vLightWeighting = uAmbientColor + uDirectionalColor*directionalLightWeighting;    
      }		
}